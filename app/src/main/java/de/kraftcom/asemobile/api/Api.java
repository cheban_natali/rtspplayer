package de.kraftcom.asemobile.api;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Api {
    @GET("android/{version}/.json")
    Call<Result> check(@Path("version") String version);
}
