package de.kraftcom.asemobile;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaRouter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import de.kraftcom.asemobile.adapter.MediaInfoAdapter;

import de.kraftcom.asemobile.util.PreferencesUtils;
import de.kraftcom.asemobile.util.Strings;


import org.videolan.libvlc.AWindow;
import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static de.kraftcom.asemobile.util.PreferencesUtils.CURR_LANGUEGE;

public class VideoPlayer extends AppCompatActivity implements IVLCVout.Callback, View.OnClickListener, View.OnTouchListener, SeekBar.OnSeekBarChangeListener/*, MediaPlayer.EventListener,Media.EventListener */ {

    @SuppressWarnings("unused")
    public static final int HW_ACCELERATION_AUTOMATIC = -1;
    public static final int HW_ACCELERATION_DISABLED = 0;
    public static final int HW_ACCELERATION_DECODING = 1;
    public static final int HW_ACCELERATION_FULL = 2;

    public static final int AFTER_WEB = 1;
    public static final int AFTER_CHANGE_CANNEL = 2;
    public static final int AFTER_REPING = 3;
    public static final int AFTER_LOST_SERVER = 4;


    private static final boolean USE_SURFACE_VIEW = true;
    //private static  boolean ENABLE_SUBTITLES = false;
    private static boolean ENABLE_SUBTITLES;

    public final static String TAG = "RtspPlayerActivity";
    public final String LOG_TAG = "Player";

    private static final int SURFACE_BEST_FIT = 0;
    private static final int SURFACE_FIT_SCREEN = 1;
    private static final int SURFACE_FILL = 2;
    private static final int SURFACE_16_9 = 3;
    private static final int SURFACE_4_3 = 4;
    private static final int SURFACE_ORIGINAL = 5;
    private static int CURRENT_SIZE = SURFACE_BEST_FIT;

    private FrameLayout mVideoSurfaceFrame = null;
    private SurfaceView mVideoSurface = null;
    private SurfaceView mSubtitlesSurfaceView = null;
    private SurfaceHolder mSubtitlesSurfaceHolder;
    private View mChannelView = null, channelSurfaceStub;
    private TextureView mVideoTexture = null;
    private View mVideoView = null;
    private RelativeLayout videoState;

    private AWindow mAWindow = null;

    private MediaRouter mMediaRouter;
    private MediaRouter.SimpleCallback mMediaRouterCallback;
    private MediaInfoAdapter mAdapter;
    private MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);

    private ViewStub textureStub, subtitlesSurfaceStub, stub;
    private final Handler mHandler = new Handler();
    private View.OnLayoutChangeListener mOnLayoutChangeListener = null;
    private int mCurrentAudioTrack = -2, mCurrentSpuTrack = -2;

    public String mFilePatch, mNameChannel,loc,mlangChannel;
    private SurfaceView mSurface;
    private SurfaceHolder holder;

    private LibVLC mLibVLC = null;
    public MediaPlayer mMediaPlayer = null;
    private int mVideoWidth = 0;
    private int mVideoHeight = 0;
    private int mVideoVisibleHeight = 0;
    private int mVideoVisibleWidth = 0;
    private int mVideoSarNum = 0;
    private int mVideoSarDen = 0;
    public IVLCVout vlcVout;
    public int countES;
    Media media;
    //  Serializable channels;
    RelativeLayout menuPlace, Upmenu;
    ImageView btPlay, btStop,btPause,btRewind,btLanguage, btForward, placeLanguage ;
    //TextView ;
    Button btExitTV, btSubTitre;
   public SeekBar vpSeekBar;
    boolean  is_menu=false, aftetUpdate, showBtPause, afterPause=false;
    ListView lvProgram;
    public TextView pvTime,vpDuration,txSpeed,currTime, currDate,titleVideo;
    Context context;
    ImageButton btExit;
    public static TextView mTextSub;
    String currentDate,currentLanguege,currentTime, initLanguege;
    Calendar currentTimeDate;
    int listPosition, sizeList ;
    MediaPlayer.Title[] titles;
    private boolean mHasSubItems = false;
    private MediaPlayer.TrackDescription[] mAudioTracksList;
  private MediaPlayer.TrackDescription[] mSubtitleTracksList;
    private final ArrayList<String> mSubtitleSelectedFiles = new ArrayList<>();

    private AlertDialog mAlertDialog;

    public int currentFlag;
    public String[] nameList;
    public int[] flagList;
    public int[] idList;
    public int ScreenWidth, ScreenHeight, btSubTitreWidth, btSubTitreHeight;


    private long mForcedTime = -1;
    private long mLastTime = -1;
    private int mLastAudioTrack = -2;
    private int mLastSpuTrack = -2;
    private SharedPreferences mSettings;
    private Uri mUri;


    SharedPreferences prefs;
    public int countNotChannel=0;
    public Boolean isBtnPlay=false,isForward=false,//1
            activationChannel = false;//4
    public int track_audio, subtitles, track_video, exit_tv, invalid_location, invalid_subtitles,
            track_text, idTrackOn, idtrackOff, channelPlaseWidth;
    public int checkBtnSub,audioTrackCount;
    public long currentTimer=0,currentTimerAfterForward=0;
    public float currentRate;
    public long timerT=15000;
    Handler   handler = new Handler();
    final Runnable intLang = new Runnable() {
        public void run() {
            handler.postDelayed(this, timerT);
            /*if(!vlcVout.areViewsAttached()){
                initVlcOut();
            }else{*/
            Log.d(LOG_TAG, "intLang run"+timerT);
            if(vlcVout.areViewsAttached()){
                Log.d(LOG_TAG, "vlcVout.areViewsAttached()"+timerT);
                initialLanguegeTrack(initLanguege);
            }
          //  }


        }
    };
    final Runnable defLang = new Runnable() {
        public void run() {
            handler.postDelayed(this, timerT);
           initialTrackDefauld();
            //initialLanguegeTrack(mlangChannel);

        }
    };
    static final Map<String , String> shortlanguegeBase = new HashMap<String , String>() {{
        put("en",   "English");
        put("fr", "French");
        put("de",   "German");
        put("it",   "Italian");
        put("es",   "Spanish");
        put("Track 1",   "Track 1");
    }};
    static final Map<String , Integer> languegeBase = new HashMap<String , Integer>() {{
        put("English",    R.drawable.en);
        put("French", R.drawable.fr);
        put("German",   R.drawable.de);
        put("Italian",   R.drawable.it);
        put("Spanish",   R.drawable.es);
        put("Track 1",   R.drawable.language_selection);
    }};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ScreenWidth = getScreenWidth();
        ScreenHeight = getScreenHeight();

        initUI();

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE

        );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mFilePatch = extras.getString("TEXT_URL");
           //mFilePatch ="http://62.204.160.84:7080/videotest/p.m3u8";
           // mFilePatch ="http://62.204.160.84:7080/videotest/8_SEKUNDEN_Trailer_2015_Fahri_Yardim.mp4";
            mNameChannel = extras.getString("TEXT_NAME");
            mlangChannel = extras.getString("TEXT_LANG");
            Log.d(LOG_TAG, "TEXT_LANG mlangChannel = "+ mlangChannel);
        }else{
           // mFilePatch ="http://62.204.160.84:7080/videotest/p.m3u8";
           // mFilePatch =  PreferencesUtils.readStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_FILE,null);
        }
        //saveChannelState(mFilePatch,mCurrentSpuTrack);
      //  mFilePatch ="http://62.204.160.84:7080/videotest/p.m3u8";
        currentLanguege= (shortlanguegeBase.get(mlangChannel)!=null)?shortlanguegeBase.get(mlangChannel):"";
        PreferencesUtils.saveStrSharedSetting(getApplicationContext(), CURR_LANGUEGE, currentLanguege);
        mUri=Uri.parse(mFilePatch);
        context = getApplicationContext();
        initPlayer();
        mMediaPlayer.setEventListener(mPlayerListener);
    }


    public void initUI() {

      menuPlace = findViewById(R.id.ll_menu_video);
        Upmenu = findViewById(R.id.ll_menu);
        btPlay= findViewById(R.id.vp_play);
        btStop= findViewById(R.id.vp_stop);
        btPause= findViewById(R.id.vp_pause);
        btRewind= findViewById(R.id.vp_rewind);
        btForward= findViewById(R.id.vp_forward);
        btLanguage= findViewById(R.id.vp_language);
        placeLanguage= findViewById(R.id.bt_language);
        vpSeekBar= findViewById(R.id.player_seekbar);
        pvTime= findViewById(R.id.player_overlay_time);
        txSpeed= findViewById(R.id.tx_speed);
        vpDuration= findViewById(R.id.player_overlay_length);

        btExit= findViewById(R.id.bt_exit);
        currTime= findViewById(R.id.curr_time);
        currTime.setText(getCurrentTime());
        currDate= findViewById(R.id.curr_date);
        currDate.setText(getCurrentData());
        titleVideo= findViewById(R.id.title_video);

        menuPlace.setVisibility(View.INVISIBLE);
        Upmenu.setVisibility(View.INVISIBLE);
        mVideoSurfaceFrame = findViewById(R.id.video_surface_frame);
        videoState = findViewById(R.id.video_state);


        btPlay.setOnClickListener(this);
        btStop.setOnClickListener(this);
        btPause.setOnClickListener(this);
        btRewind.setOnClickListener(this);
        btForward.setOnClickListener(this);
        btLanguage.setOnClickListener(this);
        btExit.setOnClickListener(this);


        btPlay.setVisibility(View.GONE);
        showBtPause=true;
        vpSeekBar.setOnSeekBarChangeListener(this);

        track_audio = R.string.track_audio;

       // btRewind.setVisibility(View.INVISIBLE);
       // btForward.setVisibility(View.INVISIBLE);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public void initPlayer() {
        final ArrayList<String> options = new ArrayList<>();
        mLibVLC = new LibVLC(context, options);//3.0.4 lib
        mMediaPlayer = new MediaPlayer(mLibVLC);
        if (USE_SURFACE_VIEW) {
            stub = findViewById(R.id.surface_stub);
            mVideoSurface = (SurfaceView) stub.inflate();
            mVideoView = mVideoSurface;
        } else {
            textureStub = findViewById(R.id.texture_stub);
            mVideoTexture = (TextureView) textureStub.inflate();
            mVideoView = mVideoTexture;
        }
        mVideoView.setOnTouchListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart");
        // is_menu=false;
        initVlcOut();
    }
public void initVlcOut(){
            activationChannel=false;
            vlcVout = mMediaPlayer.getVLCVout();

            mMediaPlayer.setEventListener(mPlayerListener);
            if (mVideoSurface != null) {
                vlcVout.setVideoView(mVideoSurface);
                if (mSubtitlesSurfaceView != null)
                    vlcVout.setSubtitlesView(mSubtitlesSurfaceView);

            } else
                vlcVout.setVideoView(mVideoTexture);

            vlcVout.addCallback(this);
            vlcVout.attachViews();
            Media media = new Media(mLibVLC, mUri);
            mMediaPlayer.setMedia(media);
            media.parse();
            media.release();
            mMediaPlayer.play();
           // mLastAudioTrack = mMediaPlayer.getAudioTrack();

            if(!TextUtils.isEmpty(mlangChannel)){
                initLanguege = (shortlanguegeBase.get(mlangChannel)!=null)?shortlanguegeBase.get(mlangChannel):"";

                Log.d(LOG_TAG, "mlangChannel = "+ mlangChannel);
                Log.d(LOG_TAG, "initLanguege = "+ initLanguege);

                if(initLanguege.isEmpty()){
                    Log.d(LOG_TAG, "initLanguege isEmpty ");
                    handler.postDelayed(defLang, timerT);
                }else{
                    handler.postDelayed(intLang, timerT);
                }
            }

            aftetUpdate=false;
            titles = mMediaPlayer.getTitles();

            titleVideo.setText(mNameChannel);

            if (mOnLayoutChangeListener == null) {
                mOnLayoutChangeListener = new View.OnLayoutChangeListener() {
                    private final Runnable mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            updateVideoSurfaces();
                        }
                    };

                    @Override
                    public void onLayoutChange(View v, int left, int top, int right,
                                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
                            mHandler.removeCallbacks(mRunnable);
                            mHandler.post(mRunnable);
                        }
                    }
                };
            }
            mVideoSurfaceFrame.addOnLayoutChangeListener(mOnLayoutChangeListener);

        }

    public String getCurrentData(){
        currentTimeDate = Calendar.getInstance();
        SimpleDateFormat df=new SimpleDateFormat("dd.MM.yyyy");
       // currentDate=df.format(currentTimeDate.getTime());
        return df.format(currentTimeDate.getTime());
    }
    public String getCurrentTime(){
        currentTimeDate = Calendar.getInstance();
        SimpleDateFormat df=new SimpleDateFormat("HH:mm");
       // currentTime=df.format(currentTimeDate.getTime());
        return df.format(currentTimeDate.getTime());
    }

    /*public void updateProgressBar() {
        mSeekbarUpdateHandler.postDelayed(mUpdateSeekbar, 100);

    }*/
    public void showMenu(Boolean show) {
        if (show) {
            menuPlace.setVisibility(View.VISIBLE);
            Upmenu.setVisibility(View.VISIBLE);
           /*getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            //  | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            //  | View.SYSTEM_UI_FLAG_FULLSCREEN
                            //    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                            | View.SYSTEM_UI_FLAG_IMMERSIVE

            );*/
            //  is_menu = true;
        } else {
            menuPlace.setVisibility(View.INVISIBLE);
            Upmenu.setVisibility(View.INVISIBLE);
           /* getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE

            );*/
            // is_menu = false;
        }
    }

    public void play(){
        Log.d(LOG_TAG, "Play btn after Forward currentTimer="+currentTimer+ " vpSeekBar.getProgress()="+vpSeekBar.getProgress());
        if((mMediaPlayer!=null)/*&&(!mMediaPlayer.isPlaying())*/){
           // savePlayerState();
            if(isForward){
                currentRate=1f;
                mMediaPlayer.setRate(currentRate);
                setRateProgress();
                isForward=false;
                //onStopTrackingTouch(vpSeekBar);
                Log.d(LOG_TAG, "Play btn after Forward currentTimer="+currentTimer+ " vpSeekBar.getProgress()="+vpSeekBar.getProgress());
              // currentTimer =currentTimerAfterForward;
            }
            if(currentTimer!=0){
               // updateAudioTrack();

                mMediaPlayer.setTime(currentTimer);
                vpSeekBar.setProgress((int)currentTimer);
                pvTime.setText(Strings.milliSecondsToTimer(currentTimer));
            }
           if(currentRate!=0){mMediaPlayer.setRate(currentRate);}
            updateInterface();
            if(mLastAudioTrack>-1){
                updateAudioTrack();
               // mMediaPlayer.setAudioTrack(mLastAudioTrack);
            }
          // updateAudioTrack();
            mMediaPlayer.play();
            btPause.setVisibility(View.VISIBLE);
            btPlay.setVisibility(View.GONE);
            showBtPause=true;
            isBtnPlay=false;
           // savePlayerState();
            Log.d(LOG_TAG, "Play btn");
        }
    }
    public void stop(){
        if((mMediaPlayer!=null)&&(mMediaPlayer.isPlaying())){
           // mMediaPlayer.stop();
           mMediaPlayer.pause();
            btPause.setVisibility(View.GONE);
            btPlay.setVisibility(View.VISIBLE);
            showBtPause=false;
            savePlayerState();
            Log.d(LOG_TAG, "Stop btn");
        }
    }
    public void pause(){
        if((mMediaPlayer!=null)&&(mMediaPlayer.isPlaying())){
            mMediaPlayer.pause();
           // afterPause=true;
            btPause.setVisibility(View.GONE);
            btPlay.setVisibility(View.VISIBLE);
            savePlayerState();
            showBtPause=false;
            isBtnPlay=true;
            Log.d(LOG_TAG, "Pause btn");
        }
    }
    public void savePlayerState(){
        Log.d(LOG_TAG, "savePlayerState");
        currentTimer= mMediaPlayer.getTime();
        Log.d(LOG_TAG, "currentTimer  " + currentTimer);
        if(btPlay.getVisibility()!=View.VISIBLE){
            Log.d(LOG_TAG, "savePlayerState showBtPause==true");
            isBtnPlay=false;
            showBtPause=true;
        }else{
            Log.d(LOG_TAG, "savePlayerState showBtPause==false");
            isBtnPlay=true;
            showBtPause=false;
        }
        currentRate=mMediaPlayer.getRate();
        audioTrackCount=mMediaPlayer.getAudioTracksCount();
        Log.d(LOG_TAG, "currentRate  " + currentRate);
        Log.d(LOG_TAG, "currentLanguege  " + currentLanguege);

        PreferencesUtils.saveStrSharedSetting(getApplicationContext(), CURR_LANGUEGE, currentLanguege);

    }
    public void changeSpeed(float delta){
        double initialRate = Math.round(mMediaPlayer.getRate() * 100d) / 100d;
        Log.d(LOG_TAG,"initialRate1 "+initialRate );
        if (delta>0)
            initialRate = Math.floor((initialRate + 0.005d) / 0.05d) * 0.05d;
        else
            initialRate = Math.ceil((initialRate - 0.005d) / 0.05d) * 0.05d;
        Log.d(LOG_TAG,"initialRate2 "+initialRate );
        float rate = Math.round((initialRate + delta) * 100f) / 100f;
        Log.d(LOG_TAG,"rate "+rate );
        if (rate < 1f || rate > 16f )
            return;
        mMediaPlayer.setRate(rate);
    }
    private void setRateProgress() {
        double speed = mMediaPlayer.getRate();
       // Log.d(LOG_TAG,"speed1 "+speed );
        speed = 100 * (1 + Math.log(speed) / Math.log(4));
       // Log.d(LOG_TAG,"speed2 "+speed );

        vpSeekBar.setProgress((int) speed);
        updateInterface();
    }

    private void updateInterface() {
        float rate = mMediaPlayer.getRate();
        txSpeed.setText(Strings.formatRateString(rate));
        }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

         case R.id.vp_play:
         //   media.getMeta(getTaskId());
             play();
                break;
        case R.id.vp_stop:
           // stop();
            finish();
            break;
        case R.id.vp_pause:
            pause();
            break;
        case R.id.vp_rewind:
            Log.d(LOG_TAG, "rewind");
           // changeSpeed(-1f);
            mMediaPlayer.setRate(1f);
            setRateProgress();
            isForward=false;
            break;
        case R.id.vp_forward:
            Log.d(LOG_TAG, "Forward");

            if(showBtPause) {
                btPause.setVisibility(View.GONE);
                btPlay.setVisibility(View.VISIBLE);
                showBtPause = false;
                isBtnPlay=true;
                isForward=true;
            }
            changeSpeed(1f);
            setRateProgress();
            break;
        case R.id.vp_language:
           // onAudioSubClick(v);
            Log.d(LOG_TAG, "Language Click");
            selectAudioTrack();

        break;
        case R.id.bt_exit:
            finish();
        break;

        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
       Log.d(LOG_TAG, "onTouch" + "   View=" + v + "   MotionEvent=" + event);
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP: // нажатие

                if (y < (ScreenHeight - btSubTitreHeight - 100)) {

                    if (!is_menu) {

                        //setScanTrack();
                        //  setBtnSub(readChannelState(mFilePatch));
                        showMenu(true);
                        is_menu = true;
                    } else {

                        showMenu(false);
                        is_menu = false;
                    }
                }
                break;
        }
        return true;
    }

    public void scanTrack(final MediaPlayer.TrackDescription[] tracks) {
        Log.d(LOG_TAG,"scanTrack");
        //setESTrackLists();
        nameList = null;
        idList = null;
        if (tracks == null) {
            return;
        }
        String[] nameList1 = new String[tracks.length];
        int[] idList1 = new int[tracks.length];
        int i = 0;
        sizeList = 0;
        listPosition = 0;
        for (MediaPlayer.TrackDescription track : tracks) {
            if ((track.id > 0) ) {
                sizeList++;

            idList1[i] = track.id;
            nameList1[i] = track.name;
            // map the track position to the list position
            if (track.id == mLastAudioTrack)
                listPosition = i;
            i++;
            }
            // }else{
        }

        nameList = new String[sizeList];
        idList = new int[sizeList];
        flagList = new int[sizeList];
        i = 0;
       int j = 0;
     while (j < tracks.length) {
            if ((idList1[j] > 0) ) {
                Log.d(LOG_TAG, "nameList1[j]="+nameList1[j].toString()+" j="+j);
                if(nameList1[j].contains("[")&&nameList1[j].contains("]")){
                    int first=nameList1[j].indexOf("[");
                    int last = nameList1[j].indexOf("]");
                    nameList[i]=nameList1[j].substring(first+1,last);
            }else{
                    if(nameList1[j].contains("Track 1")){
                        nameList[i]=nameList1[j];
                    }
                }
               Log.d(LOG_TAG, "nameList[i]="+nameList[i].toString()+" i="+i);
                idList[i] = idList1[j];
                i++;
           }
           j++;
        }
    }
    private void initialTrackDefauld() {
        if (mMediaPlayer.getAudioTracksCount() > 0) {
            scanTrack(mMediaPlayer.getAudioTracks());
        } else {
            return;
        }
        if (!isFinishing()) {
            if (sizeList > 0) {
                listPosition=0;
                currentLanguege = nameList[0];
                mLastAudioTrack = idList[0];
                // mMediaPlayer.setAudioTrack(mLastAudioTrack);
                currentFlag = languegeBase.get(currentLanguege);
                placeLanguage.setImageResource(currentFlag);

            }
            handler.removeCallbacks(defLang);
        }

    }
    //13:46:20   13:48:47

    private void initialLanguegeTrack(String lang) {
        if ( mMediaPlayer.getAudioTracksCount() > 0) {
            Log.d(LOG_TAG, "intLang + mMediaPlayer.getAudioTracksCount() > 0");
            scanTrack(mMediaPlayer.getAudioTracks());
        } else {
            Log.d(LOG_TAG, "intLang + mMediaPlayer.getAudioTracksCount() <= 0");
            return;}
        if (!isFinishing()) {
            if(sizeList>0) {
                Log.d(LOG_TAG, "intLang sizeList>0");
                listPosition=0;
                int i = 0;
                while (i < sizeList) {
                    if ((idList[i] > 0)) {
                        if (nameList[i].contains(lang)) {
                            listPosition = i;
                            break;
                        }
                    }
                    i++;
                }
                currentLanguege = nameList[listPosition];
                mLastAudioTrack = idList[listPosition];
                mMediaPlayer.setAudioTrack(mLastAudioTrack);
                currentFlag = languegeBase.get(currentLanguege);
                placeLanguage.setImageResource(currentFlag);

            }
            handler.removeCallbacks(intLang);
        }

    }

    private void selectTrack() {
        if ( mMediaPlayer.getAudioTracksCount() > 0) {
            scanTrack(mMediaPlayer.getAudioTracks());
        } else {return;}

        if (!isFinishing()) {
            if((nameList!=null) && (sizeList>0)&&(nameList.length>0)) {
                Log.d(LOG_TAG,"nameList = "+ nameList);
                if ((sizeList != 0) && (listPosition <= sizeList-2)) {
                    listPosition = listPosition + 1;

                } else {if ((sizeList != 0) && (listPosition == sizeList-1)) {
                        listPosition = 0;
                }
                }
                mLastAudioTrack = idList[listPosition];
                mMediaPlayer.setAudioTrack(mLastAudioTrack);
                currentLanguege = nameList[listPosition];
                currentFlag = languegeBase.get(currentLanguege);
                   // btLanguage.setImageResource(currentFlag);
                placeLanguage.setImageResource(currentFlag);
                PreferencesUtils.saveStrSharedSetting(getApplicationContext(), CURR_LANGUEGE, currentLanguege);
            Log.d(LOG_TAG,"selectTrack: setAudioTrack="+currentLanguege+"  listPosition="+listPosition+"  mLastAudioTrack="+mLastAudioTrack);
        }else{
               // selectTrack();
        }
        }
    }

    private void updateTrack() {
        Log.d(LOG_TAG, "updateTrack");
        if ( mMediaPlayer.getAudioTracksCount() > 0) {
            scanTrack(mMediaPlayer.getAudioTracks());
        } else {return;}
        if (!isFinishing()) {
            if(sizeList>0) {
                currentLanguege = PreferencesUtils.readStrSharedSetting(getApplicationContext(), PreferencesUtils.CURR_LANGUEGE, "English");
                //Log.d(LOG_TAG,"nameList = "+ nameList.toString());

                if(afterPause){
                    listPosition=-1;
                }
                if(currentLanguege.isEmpty()){
                    initialTrackDefauld();
                }else {
                    int i = 0;
                    while (i < sizeList) {
                        if ((idList[i] > 0)) {
                            if (nameList[i].contains(currentLanguege)) {
                                listPosition = i;
                                break;
                            }
                        }
                        i++;
                    }
                    if ((listPosition == -1) && (afterPause)) {
                        updateTrack();
                    }
                    mLastAudioTrack = idList[listPosition];

//                mMediaPlayer.setAudioTrack(mLastAudioTrack);
                    mMediaPlayer.setAudioTrack(mLastAudioTrack);

                    currentFlag = languegeBase.get(currentLanguege);
                    placeLanguage.setImageResource(currentFlag);
                    Log.d(LOG_TAG, "updateTrack: setAudioTrack=" + currentLanguege + "  listPosition=" + listPosition + "  mLastAudioTrack=" + mLastAudioTrack);
                    //PreferencesUtils.saveStrSharedSetting(getApplicationContext(), CURR_LANGUEGE, currentLanguege);

                    Log.d(LOG_TAG, "afterPause=" + afterPause);
                }

            }else{
               // updateTrack();
}
}
    }

    private void selectAudioTrack() {
        Log.d(LOG_TAG,"selectAudioTrack");
        setESTrackLists();
        selectTrack();
    }

    private void updateAudioTrack() {
        Log.d(LOG_TAG,"updateAudioTrack");
        setESTrackLists();
        //updateTrack(mAudioTracksList);
       // updateTrack(mMediaPlayer.getAudioTracks());
        updateTrack();
    }

    private  void setESTrackLists() {
        /*if (mAudioTracksList == null && mMediaPlayer.getAudioTracksCount() > 0) {
            mAudioTracksList = mMediaPlayer.getAudioTracks();
            Log.d(LOG_TAG, "mAudioTracksList = mMediaPlayer.getAudioTracks()  "+ mAudioTracksList );
        }*/
        }

    private void invalidateESTracks(int type) {
        /*switch (type) {
            case Media.Track.Type.Audio:
                Log.d(LOG_TAG, "mAudioTracksList = null   " );
                mAudioTracksList = null;
                break;
        }*/
    }

    void reping() {
        ////Log.d(LOG_TAG,"reping");
        if((mMediaPlayer!=null)&&(mUri!=null)) {
            mMediaPlayer.stop();

            mMediaPlayer.getVLCVout().detachViews();
            onStart();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy   " );
        if(intLang!=null){handler.removeCallbacks(intLang);}
        if(defLang!=null){handler.removeCallbacks(defLang);}

        savePlayerState();
        mMediaPlayer.release();
        mLibVLC.release();



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.d(LOG_TAG, "onResume   " );
       // checkBtnSub =PreferencesUtils.readBoolSharedSetting(getApplicationContext(), PreferencesUtils.CHECK_BTN_SUB, false)? 1: 0;
        mVideoView.setOnTouchListener(this);
       if(afterPause) {
        /*mMediaPlayer.setTime(currentTimer);
        mMediaPlayer.setRate(currentRate);
        vpSeekBar.setProgress((int)currentTimer);
        pvTime.setText(Strings.milliSecondsToTimer(currentTimer));
      */
        if(showBtPause==false){
            Log.d(LOG_TAG, "showBtPause==false");
            btPause.setVisibility(View.GONE);
            btPlay.setVisibility(View.VISIBLE);
            mMediaPlayer.pause();
            Log.d(LOG_TAG, "mMediaPlayer.pause()");
        }else{
            Log.d(LOG_TAG, "showBtPause==true");
            btPause.setVisibility(View.VISIBLE);
            btPlay.setVisibility(View.GONE);
            Log.d(LOG_TAG, "mMediaPlayer.play");
        }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mOnLayoutChangeListener != null) {
            mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
            mOnLayoutChangeListener = null;
        }
       Log.d(LOG_TAG, "onPause  " );
        mMediaPlayer.pause();
        afterPause=true;
        savePlayerState();
        Log.d(LOG_TAG, "onPause  " + mMediaPlayer.getTime());
       // mMediaPlayer.getVLCVout().detachViews();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mOnLayoutChangeListener != null) {
            mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
            mOnLayoutChangeListener = null;
        }
        mMediaPlayer.pause();
        Log.d(LOG_TAG, "onStop: " );
          mMediaPlayer.getVLCVout().detachViews();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser){


            pvTime.setText(String.format("%s",Strings.milliSecondsToTimer(progress)));
            vpSeekBar.setProgress((progress));
            mMediaPlayer.setTime(progress);

            savePlayerState();
            // currentTimer=progress;
            // aftetUpdate=true;
            Log.d(LOG_TAG, "onProgressChanged: fromUser " + fromUser + " progress = "+ progress+ " currentTimerAfterForward = "+ currentTimerAfterForward);
        }
        vpSeekBar.setMax((int)mMediaPlayer.getLength());
       vpDuration.setText(Strings.milliSecondsToTimer(mMediaPlayer.getLength()));

       // vpSeekBar.setProgress(progress);
        currentTimer=progress;
        Log.d(LOG_TAG, "onProgressChanged: fromUser " + fromUser + " progress = "+ progress+ " currentTimer = "+ currentTimer);
        pvTime.setText(String.format("%s",Strings.milliSecondsToTimer(progress)));
        savePlayerState();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        Log.d(LOG_TAG, "onStartTrackingTouch: ");
        mMediaPlayer.pause();
       // mSeekbarUpdateHandler.removeCallbacks(mUpdateSeekbar);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
      //  mSeekbarUpdateHandler.removeCallbacks(mUpdateSeekbar);
        Log.d(LOG_TAG, "onStopTrackingTouch   " );
        int totalDuration = (int)mMediaPlayer.getLength();
        //currentTimer = progressToTimer(seekBar.getProgress(), totalDuration);
        currentTimer = seekBar.getProgress();
        Log.d(LOG_TAG, "onStopTrackingTouch   currentTimer = "+currentTimer );
        Log.d(LOG_TAG, "onStopTrackingTouch   seekBar.getProgress()"+seekBar.getProgress() );
       // currentTimer=currentPosition;
        // forward or backward to certain seconds
        mMediaPlayer.setTime(currentTimer);

        mMediaPlayer.play();
        aftetUpdate=true;
        Log.d(LOG_TAG, "Start update  " );
        updateAudioTrack();
        Log.d(LOG_TAG, "Stop update  " );

        // update timer progress again
      //  updateProgressBar();
    }

    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private WeakReference<VideoPlayer> mOwner;

        public MyPlayerListener(VideoPlayer owner) {
            mOwner = new WeakReference<VideoPlayer>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            VideoPlayer player = mOwner.get();

            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                   player.finish();
                  //  Log.d(player.LOG_TAG,"Event EndReached");
                    break;
                case MediaPlayer.Event.Playing:
                  Log.d(player.LOG_TAG,"Event Playing");
                   // player.activationChannel=true;
                    break;
                case MediaPlayer.Event.Paused:
                    Log.d(player.LOG_TAG,"Event Paused");
                   // player.afterPause=true;
                   // Log.d(player.LOG_TAG,"Event Paused afterPause=true");
                    break;
                case MediaPlayer.Event.TimeChanged:
                 //  Log.d("Event", "onMediaPlayerEvent TimeChanged "+(int) event.getTimeChanged() );
                   //player.pvTime.setText(Strings.milliSecondsToTimer((int)(player.mMediaPlayer.getTime())));
                  // player.vpDuration.setText(Strings.milliSecondsToTimer(player.mMediaPlayer.getLength()));

                    player.vpSeekBar.setProgress((int)(player.mMediaPlayer.getTime()));
                   // player. vpSeekBar.setMax((int)player.mMediaPlayer.getLength());


                    break;
                case MediaPlayer.Event.MediaChanged:
                 //  Log.d(player.LOG_TAG,"Event MediaChanged");
                    break;
                case MediaPlayer.Event.EncounteredError:
                  //  Log.d(player.LOG_TAG,"Event EncounteredError");
                  //  player.activationChannel=false;
                    break;
                case MediaPlayer.Event.Opening:
                   Log.d(player.LOG_TAG,"Event Opening");
                  Log.d("Event Opening", "  Length "+ player.mMediaPlayer.getLength() );
                    break;
                    case MediaPlayer.Event.PositionChanged:
                // Log.d(player.LOG_TAG,"Event PositionChanged");
                        if(player.afterPause) {

                            player.updateAudioTrack();
                            Log.d(player.LOG_TAG,"Event PositionChanged afterPause==true getTime = " +(int) player.mMediaPlayer.getTime()+" currentTimer="+player.currentTimer);
                            Log.d(player.LOG_TAG," Event PositionChanged audioTrackCount = "+ player.audioTrackCount);
                            if ((((int) player.mMediaPlayer.getTime()) > (player.currentTimer)) && ((int) player.mMediaPlayer.getTime() < (player.currentTimer + 500))) {
                                Log.d(player.LOG_TAG,"Event PositionChanged updateAudioTrack");

                                //player.countES=0;
                                player.afterPause = false;
                                Log.d(player.LOG_TAG, "afterPause=" + player.afterPause);
                            }
                        }
                    break;
                case MediaPlayer.Event.Stopped:
                    Log.d(player.LOG_TAG,"Event Stopped");
                    //player.activationChannel=false;

                    break;
                case MediaPlayer.Event.Buffering:
                  Log.d(player.LOG_TAG,"Buffering");
                    break;
                case MediaPlayer.Event.ESAdded:
                    Log.d(player.LOG_TAG,"Event ESAdded");
                    if(player.afterPause) {
                        player.mMediaPlayer.setTime(player.currentTimer);
                        Log.d(player.LOG_TAG, "PositionChanged afterPause currentTimer =" + player.currentTimer);
                        player.mMediaPlayer.setRate(player.currentRate);
                        Log.d(player.LOG_TAG, "PositionChanged afterPause currentRate =" + player.currentRate);

                        player.vpSeekBar.setProgress((int) player.currentTimer);
                        player.pvTime.setText(Strings.milliSecondsToTimer(player.currentTimer));
                    }
                    break;

                case MediaPlayer.Event.ESDeleted:
                   Log.d(player.LOG_TAG," Event ESDeleted ");
                  //  if (event.getEsChangedType() == Media.Track.Type.Audio) {
                   player.invalidateESTracks(event.getEsChangedType());
                  //  }

                    break;
                case MediaPlayer.Event.ESSelected:
                 Log.d(player.LOG_TAG,"Event ESSelected");

                    if(player.aftetUpdate){
                        Log.d(player.LOG_TAG," Event ESSelected aftetUpdate = ");
                    player.updateAudioTrack();
                    }

                    break;


                default:
                    break;
            }
        }
    }


    private void updateVideoSurfaces() {
        Log.d(LOG_TAG, "updateVideoSurfaces");
        int sw = getWindow().getDecorView().getWidth();
        int sh = getWindow().getDecorView().getHeight();

        // sanity check
        if (sw * sh == 0) {
            //Log.e(TAG, "Invalid surface size");
            return;
        }

        mMediaPlayer.getVLCVout().setWindowSize(sw, sh);

        ViewGroup.LayoutParams lp = mVideoView.getLayoutParams();
        if (mVideoWidth * mVideoHeight == 0) {
            /* Case of OpenGL vouts: handles the placement of the video using MediaPlayer API */
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoView.setLayoutParams(lp);
            lp = mVideoSurfaceFrame.getLayoutParams();
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoSurfaceFrame.setLayoutParams(lp);
            return;
        }

        if (lp.width == lp.height && lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {

        }

        double dw = sw, dh = sh;

        final boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        if (sw > sh && isPortrait || sw < sh && !isPortrait) {
            dw = sh;
            dh = sw;
        }

        // compute the aspect ratio
        double ar, vw;
        if (mVideoSarDen == mVideoSarNum) {
            /* No indication about the density, assuming 1:1 */
            vw = mVideoVisibleWidth;
            ar = (double)mVideoVisibleWidth / (double)mVideoVisibleHeight;
        } else {
            /* Use the specified aspect ratio */
            vw = mVideoVisibleWidth * (double)mVideoSarNum / mVideoSarDen;
            ar = vw / mVideoVisibleHeight;
        }

        // compute the display aspect ratio
        double dar = dw / dh;

        switch (CURRENT_SIZE) {
            case SURFACE_BEST_FIT:
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_FIT_SCREEN:
                if (dar >= ar)
                    dh = dw / ar; /* horizontal */
                else
                    dw = dh * ar; /* vertical */
                break;
            case SURFACE_FILL:
                break;
            case SURFACE_16_9:
                ar = 16.0 / 9.0;
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_4_3:
                ar = 4.0 / 3.0;
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_ORIGINAL:
                dh = mVideoVisibleHeight;
                dw = vw;
                break;
        }

        // set display size
        lp.width  = (int) Math.ceil(dw * mVideoWidth / mVideoVisibleWidth);
        lp.height = (int) Math.ceil(dh * mVideoHeight / mVideoVisibleHeight);
        mVideoView.setLayoutParams(lp);
        if (mSubtitlesSurfaceView != null)
            mSubtitlesSurfaceView.setLayoutParams(lp);

        // set frame size (crop if necessary)
        lp = mVideoSurfaceFrame.getLayoutParams();
        lp.width = (int) Math.floor(dw);
        lp.height = (int) Math.floor(dh);
        mVideoSurfaceFrame.setLayoutParams(lp);

        mVideoView.invalidate();
        if (mSubtitlesSurfaceView != null)
            mSubtitlesSurfaceView.invalidate();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)

    public void onNewVideoLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        mVideoWidth = width;
        mVideoHeight = height;
        mVideoVisibleWidth = visibleWidth;
        mVideoVisibleHeight = visibleHeight;
        mVideoSarNum = sarNum;
        mVideoSarDen = sarDen;
        updateVideoSurfaces();
    }
    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    public int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    /**
     * Function to change progress to timer
     * @param progress -
     * @param totalDuration
     * returns current duration in milliseconds
     * */
    public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }
}
