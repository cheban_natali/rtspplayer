package de.kraftcom.asemobile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ErrorActivity extends AppCompatActivity implements View.OnClickListener,  MediaPlayer.EventListener {
    ImageView btReping, btSettings;
    public String mFilePatch;
    private Uri mUri;
    private URL url;
    InetAddress a;
    URLConnection ucon = null;
    private static final String TAG = "Ping";
    private static final String CMD = "/system/bin/ping -q -n -w 1 -c 1 %s";
    private static final int TIMEOUT = 1000;
    private LibVLC mLibVLC = null;
    Media mediaTest,media2;
    Context context;
    public final String LOG_TAG = "Player";
    int mediaFlags,mediaFlags2;
    private MediaPlayer mMediaPlayerTest = null;
    String mArray,mNameChannel;
    Intent intentSait, intentPlayer;
   public int volume;
    String currentDate;
    Calendar currentTime;
    SimpleDateFormat df;


    public long timerT=6000;
    Handler handler = new Handler();
    final Runnable t = new Runnable() {
        public void run() {
           handler.postDelayed(this, timerT);
            listenStateChannel();

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mArray = extras.getString("Array");
            mFilePatch = extras.getString("mFilePatch");
            mUri= Uri.parse(mFilePatch);
            mNameChannel = extras.getString("TEXT_NAME");
        }else{
            mFilePatch ="";
            mUri =null;

        }
        context = getApplicationContext();

        btReping= findViewById(R.id.bt_reping);
        btSettings= findViewById(R.id.bt_settings);

        btReping.setOnClickListener(this);
        btSettings.setOnClickListener(this);

    }
    public String getCurrentData(){
        currentTime = Calendar.getInstance();
        df=new SimpleDateFormat("HH:mm:ss");
        currentDate=df.format(currentTime.getTime());
        return currentDate;
    }
    private void listenStateChannel(){
       // Log.d(LOG_TAG, "Time listenStateChannel" +getCurrentData() );
       // Log.d(LOG_TAG," getVideoTracksCount "+String.valueOf(mMediaPlayerTest.getVideoTracksCount()));

        handler.removeCallbacks(t);
        if(mMediaPlayerTest.getVideoTracksCount()<=0){
           // Log.d(LOG_TAG, "Time intentSait" +getCurrentData() );

            mMediaPlayerTest.setVolume(volume);
            mMediaPlayerTest.stop();
            mMediaPlayerTest.release();

            intentSait = new Intent(ErrorActivity.this, MainActivity.class);
            finish();
            startActivity(intentSait);

        }else{
          //  Log.d(LOG_TAG, "Time intentPlayer" +getCurrentData() );
            mMediaPlayerTest.setVolume(volume);
            mMediaPlayerTest.stop();

            mMediaPlayerTest.release();
            intentPlayer = new Intent(ErrorActivity.this, RtspPlayerActivity.class);
            intentPlayer.putExtra("TEXT_URL", mFilePatch);
            intentPlayer.putExtra("Array", mArray);
            intentPlayer.putExtra("TEXT_NAME", mNameChannel);
            finish();
            startActivity(intentPlayer);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bt_reping:
                //startActivity(getIntent());
                if(mFilePatch!=""|| mUri!=null){

                    //  mFilePatch="http://sochi-strk.ru:1936/strk/strk.stream/playlist.m3u8";

                 // mFilePatch="rtsp://192.168.254.193:554/?src=1&freq=12545&pol=h&ro=0.35&msys=dvbs&mtype=qpsk&plts=off&sr=22000&fec=56&pids=0,17,18,97,511,512,33";
                     //+++++++++++++++++++++++++++//
                    final ArrayList<String> options = new ArrayList<>();
                    mLibVLC = new LibVLC(context, options);//3.0.4 lib
                    mMediaPlayerTest = new MediaPlayer(mLibVLC);
                    mMediaPlayerTest.setEventListener(this);
                    mediaTest = new Media(mLibVLC, Uri.parse(mFilePatch));
                    mMediaPlayerTest.setMedia(mediaTest);
                    mediaTest.parse();
                    mediaTest.release();
                    volume=mMediaPlayerTest.getVolume();
                    mMediaPlayerTest.setVolume(0);
                    mMediaPlayerTest.play();

                //    Log.d(LOG_TAG, "Time" +getCurrentData() );
                    handler.postDelayed(t, timerT);

                    //+++++++++++++++++++++++//

                }
                break;
            case R.id.bt_settings:
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                break;
        }
    }



    @Override
    public void onEvent(MediaPlayer.Event event) {
        switch (event.type) {
            case MediaPlayer.Event.Buffering:

              //  Log.d(LOG_TAG, "Buffering" + event.getBuffering());//float
              //  Log.d(LOG_TAG, "getVoutCount" + event.getVoutCount());//int

                break;
        }
    }
}
