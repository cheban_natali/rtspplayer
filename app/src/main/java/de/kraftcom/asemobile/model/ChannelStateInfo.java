package de.kraftcom.asemobile.model;

public class ChannelStateInfo {

    private String channelUrl;
    private int currentSub;
    private Boolean stateBtnSub;


    public ChannelStateInfo(String channelUrl, int currentSub, Boolean stateBtnSub) {
        this.channelUrl = channelUrl;
        this.currentSub = currentSub;
        this.stateBtnSub = stateBtnSub;
    }

    public String getChannelUrl() {
        return channelUrl;
    }

    public void setChannelUrl(String channelUrl) {
        this.channelUrl = channelUrl;
    }

    public int getCurrentSub() {
        return currentSub;
    }

    public void setCurrentSub(int currentSub) {
        this.currentSub = currentSub;
    }

    public Boolean getStateBtnSub() {
        return stateBtnSub;
    }

    public void setStateBtnSub(Boolean stateBtnSub) {
        this.stateBtnSub = stateBtnSub;
    }


}
