package de.kraftcom.asemobile.model;


import java.io.Serializable;

public class ChannelList implements Serializable {

    private String id;

    private String title;

    private String url;
    private String lang;
    private String type;
    private String ismenu;

    public ChannelList(String id,String channelTitle, String channelUrl, String channelLang, String channelType, String ismenu) {
        this.id = id;
        this.title = channelTitle;
        this.url = channelUrl;
        this.lang = channelLang;
        this.type = channelType;
        this.ismenu = ismenu;
    }
    public ChannelList(String channelTitle, String channelUrl) {
        this.title = channelTitle;
        this.url = channelUrl;

    }

    public ChannelList(){
    }

    public ChannelList(String id, String channelTitle, String channelUrl) {
        this.title = channelTitle;
        this.url = channelUrl;
        this.id = id;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setChannelTitle(String channelTitle) {
        this.title = channelTitle;
    }

    public String getChannelTitle() {
        return title;
    }

    public String getChannelUrl() {
        return url;
    }

    public void setChannelUrl(String channelUrl) {
        this.url = channelUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIsmenu() {
        return ismenu;
    }

    public void setIsmenu(String ismenu) {
        this.ismenu = ismenu;
    }
}
