
package de.kraftcom.asemobile.model.modelEpg;



import java.io.Serializable;

public class Programme implements Serializable
{

    private String channelId;

    private String progTitleLang;

    private String progTitleText;

    private String dateStart;

    private String timeStart;

    private String dateStop;

    private String timeStop;

    private String timeSdvig;

    public Programme() {
    }


    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getDateStop() {
        return dateStop;
    }

    public void setDateStop(String dateStop) {
        this.dateStop = dateStop;
    }

    public String getTimeStop() {
        return timeStop;
    }

    public void setTimeStop(String timeStop) {
        this.timeStop = timeStop;
    }

    public String getTimeSdvig() {
        return timeSdvig;
    }

    public void setTimeSdvig(String timeSdvig) {
        this.timeSdvig = timeSdvig;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getProgTitleLang() {
        return progTitleLang;
    }

    public void setProgTitleLang(String progTitleLang) {
        this.progTitleLang = progTitleLang;
    }

    public String getProgTitleText() {
        return progTitleText;
    }

    public void setProgTitleText(String progTitleText) {
        this.progTitleText = progTitleText;
    }

}
