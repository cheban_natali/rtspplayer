package de.kraftcom.asemobile.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import de.kraftcom.asemobile.model.modelEpg.ChannelEpg;
import de.kraftcom.asemobile.model.modelEpg.Programme;

import java.util.ArrayList;
import java.util.List;



public class DataBaseHandel extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "channelManager";

    // Channels table name
    public static final String TAB_CHANNELS = "Channels";

    // Channels Table Columns names
    private static final String KEY_ID = "id";
    private static final String CHN_ID = "channelId";
    private static final String CHN_NAME_LANG = "channelNameLang";
    private static final String CHN_NAME_TEXT = "channelNameText";
    private static final String CHN_ICON_SCR = "channelIconSrc";
    private static final String CHN_URL = "channelUrl";


    // Programme table name
     public static final String TAB_PROGRAMMES = "Programmes";

    // Programme Table Columns names
    private static final String ID = "id";
    private static final String PRG_CHN_ID = "channelId";
    private static final String PRG_TITLE_LANG = "progTitleLang";
    private static final String PRG_TITLE_TEXT = "progTitleText";
    private static final String DATE_START = "dateStart";
    private static final String TIME_START = "timeStart";
    private static final String DATE_STOP = "dateStop";
    private static final String TIME_STOP = "timeStop";
    private static final String TIME_SDVIG = "timeSdvig";
    /*private static final String PRG_DESC_LANG = "progDescLang";
    private static final String PRG_DESC_TEXT = "progDescText";
    private static final String PRG_CAT_LANG = "progCategoryLang";
    private static final String PRG_CAT_TEXT = "progCategoryText";*/


    public DataBaseHandel(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CHANNELS_TABLE = " CREATE TABLE IF NOT EXISTS " + TAB_CHANNELS + " ("+
               KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CHN_ID + " TEXT, " +
                CHN_NAME_LANG + " TEXT, " +
                CHN_NAME_TEXT + " TEXT, " +
                CHN_ICON_SCR + " TEXT, " +
                CHN_URL + " TEXT);";

        db.execSQL(CREATE_CHANNELS_TABLE);

        String CREATE_PROGRAMM_TABLE = " CREATE TABLE IF NOT EXISTS " + TAB_PROGRAMMES + " (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                PRG_CHN_ID + " TEXT, " +
                PRG_TITLE_LANG + " TEXT, " +
                PRG_TITLE_TEXT + " TEXT, " +
                DATE_START + " TEXT, " +
                TIME_START + " TEXT, " +
                DATE_STOP + " TEXT, " +
                TIME_STOP + " TEXT, " +
                TIME_SDVIG + " TEXT);";
                /*PRG_DESC_LANG + " TEXT" + PRG_DESC_TEXT + " TEXT"
                + PRG_CAT_LANG + " TEXT" + PRG_CAT_TEXT + " TEXT" +*/
        db.execSQL(CREATE_PROGRAMM_TABLE);

    }



    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL(" DROP TABLE IF EXISTS " + TAB_CHANNELS);////////////////
        db.execSQL(" DROP TABLE IF EXISTS " + TAB_PROGRAMMES);////////////////

        // Create tables again
        onCreate(db);
    }


    public void addChannel(ChannelEpg channel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(CHN_ID, channel.getChannelId());
        values.put(CHN_NAME_LANG, channel.getChannelNameLang());
        values.put(CHN_NAME_TEXT, channel.getChannelNameText());
        values.put(CHN_ICON_SCR, channel.getChannelIconSrc());
        values.put(CHN_URL, channel.getChannelUrl());

        // Inserting Row
        db.insert(TAB_CHANNELS, null, values);
        db.close(); // Closing database connection
    }

    public void addProgramme(Programme programme) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(PRG_CHN_ID, programme.getChannelId());
        values.put(PRG_TITLE_LANG, programme.getProgTitleLang());
        values.put(PRG_TITLE_TEXT, programme.getProgTitleText());
        values.put(DATE_START, programme.getDateStart());
        values.put(TIME_START, programme.getTimeStart());
        values.put(DATE_STOP, programme.getDateStop());
        values.put(TIME_STOP, programme.getTimeStop());
        values.put(TIME_SDVIG, programme.getTimeSdvig());
        /*values.put(PRG_DESC_LANG, programme.getProgDescLang());
      values.put(PRG_DESC_TEXT, programme.getProgDescText());
        values.put(PRG_CAT_LANG, programme.getProgCategoryLang());
        values.put(PRG_CAT_TEXT, programme.getProgCategoryText());*/

        // Inserting Row
        db.insert(TAB_PROGRAMMES, null, values);
        db.close(); // Closing database connection
    }

    public List<ChannelEpg> getAllChannel() {
        List<ChannelEpg> channelList = new ArrayList<ChannelEpg>();
        // Select All Query
        String selectQuery = " SELECT  * FROM " + TAB_CHANNELS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ChannelEpg channel = new ChannelEpg();
                channel.setChannelId(cursor.getString(cursor.getColumnIndexOrThrow(CHN_ID)));
                channel.setChannelNameLang(cursor.getString(cursor.getColumnIndexOrThrow(CHN_NAME_LANG)));
                channel.setChannelNameText(cursor.getString(cursor.getColumnIndexOrThrow(CHN_NAME_TEXT)));
                channel.setChannelIconSrc(cursor.getString(cursor.getColumnIndexOrThrow(CHN_ICON_SCR)));
                channel.setChannelUrl(cursor.getString(cursor.getColumnIndexOrThrow(CHN_URL)));
                // Adding  to list
                channelList.add(channel);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        // return  list
        return channelList;
    }

    public List<Programme> getAllProgramme() {
        List<Programme> programmeList = new ArrayList<Programme>();
        // Select All Query
        String selectQuery = " SELECT  * FROM " + TAB_PROGRAMMES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Programme programme = new Programme();
                programme.setChannelId(cursor.getString(cursor.getColumnIndexOrThrow(PRG_CHN_ID)));
                programme.setProgTitleLang(cursor.getString(cursor.getColumnIndexOrThrow(PRG_TITLE_LANG)));
                programme.setProgTitleText(cursor.getString(cursor.getColumnIndexOrThrow(PRG_TITLE_TEXT)));
                programme.setDateStart(cursor.getString(cursor.getColumnIndexOrThrow(DATE_START)));
                programme.setTimeStart(cursor.getString(cursor.getColumnIndexOrThrow(TIME_START)));
                programme.setDateStop(cursor.getString(cursor.getColumnIndexOrThrow(DATE_STOP)));
                programme.setTimeStop(cursor.getString(cursor.getColumnIndexOrThrow(TIME_STOP)));
                programme.setTimeSdvig(cursor.getString(cursor.getColumnIndexOrThrow(TIME_SDVIG)));
               /* programme.setProgDescLang(cursor.getString(cursor.getColumnIndexOrThrow(PRG_DESC_LANG)));
                programme.setProgDescText(cursor.getString(cursor.getColumnIndexOrThrow(PRG_DESC_TEXT)));
                programme.setProgCategoryLang(cursor.getString(cursor.getColumnIndexOrThrow(PRG_CAT_LANG)));
                programme.setProgCategoryText(cursor.getString(cursor.getColumnIndexOrThrow(PRG_CAT_TEXT)));*/

                // Adding  to list
                programmeList.add(programme);
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        // return  list
        return programmeList;
    }


}
