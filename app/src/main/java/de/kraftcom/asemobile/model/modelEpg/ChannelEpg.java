package de.kraftcom.asemobile.model.modelEpg;



import java.io.Serializable;

public class ChannelEpg implements Serializable {


    private String channelId;


    private String channelNameLang;


    private String channelNameText;


    private String channelUrl;

    private String channelIconSrc;

    public ChannelEpg() {
    }

    public ChannelEpg(String channelId, String channelNameLang, String channelNameText, String channelUrl, String channelIconSrc) {
        this.channelId = channelId;
        this.channelNameLang = channelNameLang;
        this.channelNameText = channelNameText;
        this.channelUrl = channelUrl;
        this.channelIconSrc = channelIconSrc;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelNameLang() {
        return channelNameLang;
    }

    public void setChannelNameLang(String channelNameLang) {
        this.channelNameLang = channelNameLang;
    }

    public String getChannelNameText() {
        return channelNameText;
    }

    public void setChannelNameText(String channelNameText) {
        this.channelNameText = channelNameText;
    }

    public String getChannelIconSrc() {
        return channelIconSrc;
    }

    public void setChannelIconSrc(String channelIconSrc) {
        this.channelIconSrc = channelIconSrc;
    }

    public String getChannelUrl() {
        return channelUrl;
    }

    public void setChannelUrl(String channelUrl) {
        this.channelUrl = channelUrl;
    }
}
