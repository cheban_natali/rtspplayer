package de.kraftcom.asemobile.model;

import java.net.URL;


class ItemChannelsReceive {
    String title;
    URL url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
}
