package de.kraftcom.asemobile.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.kraftcom.asemobile.model.ChannelStateInfo;


public class PreferencesUtils {
    public final static String VIDEO_SUBTITLE_FILES = "VideoSubtitleFiles";
    public static final String PREFERENCES_FILE = "rtspplayer";
    public final static String VIDEO_FILE = "VideoFile";
    public final static String LOCALE = "Locale";
    public final static String CHANNEL_INFO = "ChannelInfo";
    public final static String CHECK_BTN_SUB = "checkBtnSub";
    public final static String CURR_LANGUEGE = "currentLanguege";
    public static String value;

    public static void saveChannelStateInfo(Context ctx, String settingName, HashMap<String, String> inputMaps) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String inputMapsString = gson.toJson(inputMaps);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, inputMapsString);

        editor.apply();
    }

    public static String readChannelStateInfo(Context ctx, String settingName, String key, String defaultValue) {
        value = defaultValue;
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String outputMapsString = sharedPref.getString(settingName, defaultValue);
        Type type = new TypeToken<HashMap<String, String>>() {
        }.getType();

        if (type == null) {
            return value;
        }
        try {
            HashMap<String, String> outputMaps = gson.fromJson(outputMapsString, type);
            value = outputMaps.get(key);

        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return value;
    }


    /*public static void saveChannelStateInfo(Context ctx, List<ChannelStateInfo> dataList) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String dataListStr = gson.toJson(dataList);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("dataList", dataListStr);
        editor.apply();
    }

    public static List<ChannelStateInfo> readChannelStateInfo(Context context) {
        List<ChannelStateInfo> catalog = new ArrayList<ChannelStateInfo>();
        SharedPreferences mPrefs = context.getSharedPreferences(PREFERENCES_FILE, context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("dataList", "");
        if (json.isEmpty()) {
            catalog = new ArrayList<ChannelStateInfo>();
        } else {
            Type type = new TypeToken<List<ChannelStateInfo>>() { }.getType();
            catalog = gson.fromJson(json, type);
        }
        return catalog;
    }*/

    public static void saveStrSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static String readStrSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }

    public static void saveIntSharedSetting(Context ctx, String settingName, int settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(settingName, settingValue);
        editor.apply();
    }

    public static int readIntSharedSetting(Context ctx, String settingName, Integer defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
       // return Integer.valueOf(sharedPref.getString(settingName, defaultValue));
        return sharedPref.getInt(settingName, defaultValue);

    }

    public static void saveBoolSharedSetting(Context ctx, String settingName, Boolean settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(settingName, settingValue);
        editor.apply();
    }

    public static Boolean readBoolSharedSetting(Context ctx, String settingName, Boolean defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        // return Integer.valueOf(sharedPref.getString(settingName, defaultValue));
        return sharedPref.getBoolean(settingName, defaultValue);

    }
    public static void clearSharedSettingKey(Context ctx, String settingName) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(settingName);
        editor.apply();

    }
}
