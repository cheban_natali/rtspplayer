package de.kraftcom.asemobile;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PixelFormat;
import android.media.MediaRouter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import de.kraftcom.R;
import de.kraftcom.asemobile.adapter.ChannelRecyclerAdapter;
import de.kraftcom.asemobile.adapter.MediaInfoAdapter;

import de.kraftcom.asemobile.model.ChannelList;
import de.kraftcom.asemobile.model.ChannelStateInfo;
import de.kraftcom.asemobile.model.DataBaseHandel;
import de.kraftcom.asemobile.model.MyApp;
import de.kraftcom.asemobile.model.RecyclerItemClickListener;
import de.kraftcom.asemobile.model.modelEpg.ChannelEpg;
import de.kraftcom.asemobile.model.modelEpg.Programme;
import de.kraftcom.asemobile.util.PreferencesUtils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.videolan.libvlc.AWindow;
import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.VLCUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static de.kraftcom.asemobile.util.PreferencesUtils.CHANNEL_INFO;
import static de.kraftcom.asemobile.util.PreferencesUtils.CHECK_BTN_SUB;

public class RtspPlayerActivity extends AppCompatActivity implements IVLCVout.Callback, View.OnClickListener, View.OnTouchListener/*, MediaPlayer.EventListener,Media.EventListener */ {

    @SuppressWarnings("unused")
    public static final int HW_ACCELERATION_AUTOMATIC = -1;
    public static final int HW_ACCELERATION_DISABLED = 0;
    public static final int HW_ACCELERATION_DECODING = 1;
    public static final int HW_ACCELERATION_FULL = 2;

    public static final int AFTER_WEB = 1;
    public static final int AFTER_CHANGE_CANNEL = 2;
    public static final int AFTER_REPING = 3;
    public static final int AFTER_LOST_SERVER = 4;


    private static final boolean USE_SURFACE_VIEW = true;
    //private static  boolean ENABLE_SUBTITLES = false;
    private static boolean ENABLE_SUBTITLES;

    public final static String TAG = "RtspPlayerActivity";
    public final String LOG_TAG = "Player";

    private static final int SURFACE_BEST_FIT = 0;
    private static final int SURFACE_FIT_SCREEN = 1;
    private static final int SURFACE_FILL = 2;
    private static final int SURFACE_16_9 = 3;
    private static final int SURFACE_4_3 = 4;
    private static final int SURFACE_ORIGINAL = 5;
    private static int CURRENT_SIZE = SURFACE_BEST_FIT;

    private FrameLayout mVideoSurfaceFrame = null;
    private SurfaceView mVideoSurface = null;
    private SurfaceView mSubtitlesSurfaceView = null;
    private SurfaceHolder mSubtitlesSurfaceHolder;
    private View mChannelView = null, channelSurfaceStub;
    private TextureView mVideoTexture = null;
    private View mVideoView = null;
    private RelativeLayout videoState;

    private AWindow mAWindow = null;
    private ParseTracksTask mParseTracksTask = null;
    private MediaRouter mMediaRouter;
    private MediaRouter.SimpleCallback mMediaRouterCallback;
    private MediaInfoAdapter mAdapter;
    private MediaPlayer.EventListener mPlayerListener = new MyPlayerListener(this);

    private ViewStub textureStub, subtitlesSurfaceStub, stub;
    private final Handler mHandler = new Handler();
    private View.OnLayoutChangeListener mOnLayoutChangeListener = null;
    private int mCurrentAudioTrack = -2, mCurrentSpuTrack = -2;

    public String mFilePatch, mNameChannel,loc, mlangChannel,mChannelType;
    private SurfaceView mSurface;
    private SurfaceHolder holder;

    private LibVLC mLibVLC = null;
    private MediaPlayer mMediaPlayer = null;
    private int mVideoWidth = 0;
    private int mVideoHeight = 0;
    private int mVideoVisibleHeight = 0;
    private int mVideoVisibleWidth = 0;
    private int mVideoSarNum = 0;
    private int mVideoSarDen = 0;
    public IVLCVout vlcVout;
    Media media;
    //  Serializable channels;
    ArrayList<ChannelList> channels;
    //TextView ;
    Button btExitTV, btSubTitre;
    RecyclerView channelRecycler;//, infoSubRecycler;
    LinearLayout channelPlace,  placeSub, errorPlace, llSub;
    TextView textError, tvChannelName;
    boolean is_bt_menu = false, is_menu=true;
    //ListView lvProgram;
   // ArrayList<String> programma;
    //public DataBaseHandel dataBaseHandel;
    SQLiteDatabase db;
    String currentDate;
    Calendar currentTime;

    Context context;
    String mArray;
    public static TextView mTextSub;
   // List<ChannelEpg> channelList = new ArrayList<ChannelEpg>();
   /// List<Programme> programmelList = new ArrayList<Programme>();
    List<ChannelStateInfo> channelStateInfoList;

    private boolean mHasSubItems = false;
    private MediaPlayer.TrackDescription[] mAudioTracksList;
    private MediaPlayer.TrackDescription[] mSubtitleTracksList;
    private final ArrayList<String> mSubtitleSelectedFiles = new ArrayList<>();
    private boolean mPlaybackStarted = false;
    private AlertDialog mAlertDialog;

    public int currentTrack, listPosition;
    public String[] nameList;
    public int[] idList;
    public int ScreenWidth, ScreenHeight, btSubTitreWidth, btSubTitreHeight;


    private long mForcedTime = -1;
    private long mLastTime = -1;
    private int mLastAudioTrack = -2;
    private int mLastSpuTrack = -2;
    private SharedPreferences mSettings;
    private Uri mUri;
    public HashMap<String, String> channelStateInfo = new HashMap<String, String>();
    private SubtitlesGetTask mSubtitlesGetTask = null;
    SharedPreferences prefs;
    public int countNotChannel=0;
    public Boolean startAfterWeb=false,//1
            startAfterChangeCannel=false,//2
            startAfterReping=false,//3
            startAfterLostServer=false,
            isSaveChannel=false,
            btnIsSingle = false,
            btnIsAll = false,
            btnNoActive = false,
            activationChannel = false;//4
    public int track_audio, subtitles, track_video, exit_tv, invalid_location, invalid_subtitles,
            track_text, idTrackOn, idtrackOff, error_text;
    public int checkBtnSub;
public long timer=5000;
public long timerT=2200;

    Handler   handler = new Handler();

    final Runnable r = new Runnable() {
        public void run() {
            handler.postDelayed(this, timer);
            statusCheck();

        }
    };
    final Runnable t = new Runnable() {
        public void run() {
            handler.postDelayed(this, timerT);
            RecyclerItemClic();

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rtsp_player_new);
        ScreenWidth = getScreenWidth();
        ScreenHeight = getScreenHeight();
        prefs = getSharedPreferences(PreferencesUtils.PREFERENCES_FILE, 0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mArray = extras.getString("Array");
            mFilePatch = extras.getString("TEXT_URL");
            mNameChannel = extras.getString("TEXT_NAME");
            mlangChannel = extras.getString("TEXT_LANG");
            mChannelType = extras.getString("TEXT_TYPE");
            is_menu = Boolean.parseBoolean(extras.getString("IS_MENU"));
            isSaveChannel=false;
            Log.d(LOG_TAG, "TEXT_LANG mlangChannel = "+ mlangChannel);

        }else{
            mFilePatch =  PreferencesUtils.readStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_FILE,null);
        }

        //mFilePatch =  "rtsp://192.168.254.193:554/?src=1&freq=11494&pol=h&ro=0.35&msys=dvbs2&mtype=8psk&plts=on&sr=22000&fec=23&pids=0,17,18,5100,5101,5102,5104";
       // mFilePatch =  "http://sochi-strk.ru:1936/strk/strk.stream/playlist.m3u8";

      //  channels.add("0","Россия 24 ","http://46.174.255.8:8000/udp/239.192.3.71:1234/","ru","video");

       // String id,String channelTitle, String channelUrl, String channelLang, String channelType
        loc=PreferencesUtils.readStrSharedSetting(getApplicationContext(), PreferencesUtils.LOCALE,null);
        ENABLE_SUBTITLES   = PreferencesUtils.readBoolSharedSetting(getApplicationContext(), PreferencesUtils.CHECK_BTN_SUB, false);
        checkBtnSub = ENABLE_SUBTITLES ? 1: 0;

        setLanguege(loc);

        initUI();
        getCurrentData();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

        );
       // dataBaseHandel = new DataBaseHandel(this);
       // db = dataBaseHandel.getReadableDatabase();


        //saveChannelState(mFilePatch,mCurrentSpuTrack);

        mUri=Uri.parse(mFilePatch);
        channels = getChannelsWeb(mArray);

      /*  channelList = dataBaseHandel.getAllChannel();
        programmelList = dataBaseHandel.getAllProgramme();
        db.close();*/
        context = getApplicationContext();

        initPlayer();

        btExitTV.setOnClickListener(this);
        btSubTitre.setOnClickListener(this);


        mMediaPlayer.setEventListener(mPlayerListener);


        channelRecycler.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                errorPlace.setVisibility(View.INVISIBLE);
                tvChannelName.setText("");
                mFilePatch = channels.get(position).getChannelUrl();
                mChannelType= channels.get(position).getType();
                mNameChannel=channels.get(position).getTitle();

                mUri=Uri.parse(mFilePatch);
                if(vlcVout.areViewsAttached()){
                    if(mVideoSurface.getVisibility()==View.VISIBLE){mVideoSurface.setVisibility(View.INVISIBLE);}
                    vlcVout.detachViews();
                }
                if(!mChannelType.equals("radio")) {

                    btSubTitre.setVisibility(View.VISIBLE);
                    if(mVideoSurface.getVisibility()==View.INVISIBLE){mVideoSurface.setVisibility(View.VISIBLE);}
                    mVideoView = mVideoSurface;
                    vlcVout.setVideoView(mVideoSurface);
                    vlcVout.attachViews();
                    if (mSubtitlesSurfaceView != null)
                        vlcVout.setSubtitlesView(mSubtitlesSurfaceView);



                }else{
                    tvChannelName.setText(mNameChannel);
                    btSubTitre.setVisibility(View.INVISIBLE);
                    mVideoView = mVideoTexture;
                    vlcVout.setVideoView(mVideoTexture);

                }
                if (r != null) {
                    handler.removeCallbacks(r);
                }
                handler.postDelayed(r, timer);
                media = new Media(mLibVLC, mUri);
                mMediaPlayer.setMedia(media);
                media.parse();
                media.release();
                mMediaPlayer.play();

                activationChannel = false;
                isSaveChannel=false;
                getSubtitles();
              //  RecyclerItemClic();

             // Log.d(LOG_TAG, "channelRecycler : " + "mCurrentSpuTrack= " + Integer.toString(mCurrentSpuTrack) + " mFilePatch=" + mFilePatch);

               /*  mNameChannel = channels.get(position).getChannelTitle();
                getProgram(mNameChannel);
                ArrayAdapter<String> progAdapter = new ArrayAdapter(getApplicationContext(), R.layout.item_program_list, programma);
               programPlace.setVisibility(View.VISIBLE);
                lvProgram.setVisibility(View.VISIBLE);
                lvProgram.setAdapter(progAdapter);*/

                flagsStart(AFTER_CHANGE_CANNEL);

                handler.postDelayed(t, timerT);


            }

        }));
        PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_SUBTITLE_FILES, null);

            handler.postDelayed(r, timer);

        if(mChannelType.equals("radio")) {
            showMenu(true);
            tvChannelName.setText(mNameChannel);
        } else{
            showMenu(is_menu);
            tvChannelName.setText("");
        }


    }

    public void saveChannelInPrefs(String channelUri){
        isSaveChannel=true;
        PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_FILE, channelUri);
    }
    public void RecyclerItemClic(){
        setScanTrack();
    }

    public void setLanguege(String lang) {
        if ((lang == null) || lang.equals("EN")) {
            track_audio = R.string.track_audio;
            subtitles = R.string.subtitles;
            exit_tv = R.string.exit_tv;
            error_text = R.string.error_channel;
            invalid_location = R.string.invalid_location;
            invalid_subtitles = R.string.invalid_subtitles;
            MediaInfoAdapter.track_video = R.string.track_video;
            MediaInfoAdapter.track_audio = R.string.track_audio;
            MediaInfoAdapter.track_text = R.string.track_text;
            track_text = R.string.track_text;
            MediaInfoAdapter.track_unknown = R.string.track_unknown;
            MediaInfoAdapter.track_bitrate_info = R.string.track_bitrate_info;
            MediaInfoAdapter.track_codec_info = R.string.track_codec_info;
            MediaInfoAdapter.track_channels_info_quantity = R.plurals.track_channels_info_quantity;
            MediaInfoAdapter.track_language_info = R.string.track_language_info;
            MediaInfoAdapter.track_samplerate_info = R.string.track_samplerate_info;
            MediaInfoAdapter.track_resolution_info = R.string.track_resolution_info;
            MediaInfoAdapter.track_framerate_info = R.string.track_framerate_info;

        } else {
            track_audio = R.string.track_audio;
            subtitles = R.string.subtitles;
            exit_tv = R.string.exit_tv;
            error_text = R.string.error_channel;
            invalid_location = R.string.invalid_location;
            invalid_subtitles = R.string.invalid_subtitles;
            MediaInfoAdapter.track_video = R.string.track_video;
            MediaInfoAdapter.track_audio = R.string.track_audio;
            MediaInfoAdapter.track_text = R.string.track_text;
            track_text = R.string.track_text;
            MediaInfoAdapter.track_unknown = R.string.track_unknown;
            MediaInfoAdapter.track_bitrate_info = R.string.track_bitrate_info;
            MediaInfoAdapter.track_codec_info = R.string.track_codec_info;
            MediaInfoAdapter.track_channels_info_quantity = R.plurals.track_channels_info_quantity;
            MediaInfoAdapter.track_language_info = R.string.track_language_info;
            MediaInfoAdapter.track_samplerate_info = R.string.track_samplerate_info;
            MediaInfoAdapter.track_resolution_info = R.string.track_resolution_info;
            MediaInfoAdapter.track_framerate_info = R.string.track_framerate_info;
           /* track_audio = R.string.track_audio_de;
            subtitles = R.string.subtitles_de;
            exit_tv = R.string.exit_tv_de;
            invalid_location = R.string.invalid_location_de;
            invalid_subtitles = R.string.invalid_subtitles_de;
            MediaInfoAdapter.track_video = R.string.track_video_de;
            MediaInfoAdapter.track_audio = R.string.track_audio_de;
            MediaInfoAdapter.track_text = R.string.track_text_de;
            track_text = R.string.track_text_de;
            MediaInfoAdapter.track_unknown = R.string.track_unknown_de;
            MediaInfoAdapter.track_bitrate_info = R.string.track_bitrate_info_de;
            MediaInfoAdapter.track_codec_info = R.string.track_codec_info_de;
            MediaInfoAdapter.track_channels_info_quantity = R.plurals.track_channels_info_quantity_de;
            MediaInfoAdapter.track_language_info = R.string.track_language_info_de;
            MediaInfoAdapter.track_samplerate_info = R.string.track_samplerate_info_de;
            MediaInfoAdapter.track_resolution_info = R.string.track_resolution_info_de;
            MediaInfoAdapter.track_framerate_info = R.string.track_framerate_info_de;*/

        }
    }


    public void statusCheck(){
        if(!mChannelType.equals("radio")){
            if((mMediaPlayer!=null)&&(mUri!=null)&& (mMediaPlayer.isPlaying())&&(activationChannel==true)&& (mMediaPlayer.getVideoTracksCount()!=0) ) {
                countNotChannel=0;
                if (isSaveChannel == false) {
                    saveChannelInPrefs(mFilePatch);
                }
            }else{
                if((activationChannel==true)&&(mMediaPlayer.getVideoTracksCount()!=0)){
                    if(r!=null){
                        handler.removeCallbacks(r);
                    }
                    countNotChannel=0;
                    finish();
                    startActivity(getIntent());
                }else {
                    if(mMediaPlayer.getVideoTracksCount()==0){
                        countNotChannel++;
                        if (countNotChannel == 3) {
                           mMediaPlayer.stop();
                            if(r!=null){
                                handler.removeCallbacks(r);
                            }
                            errorPlace.setVisibility(View.VISIBLE);
                                /*Intent intenterr = new Intent(RtspPlayerActivity.this, ErrorActivity.class);
                                intenterr.putExtra("mFilePatch", mFilePatch);
                                intenterr.putExtra("Array", mArray);
                                intenterr.putExtra("TEXT_NAME", mNameChannel);
                                intenterr.putExtra("TEXT_LANG", mlangChannel);
                                intenterr.putExtra("TEXT_TYPE", mChannelType);
                                finish();
                                startActivity(intenterr);*/

                               // Toast.makeText(this, getResources().getText(invalid_location), Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                }


        }else
            if(mChannelType.equals("radio")){
                if((mMediaPlayer!=null)&&(mUri!=null)&& (mMediaPlayer.getAudioTracksCount()==0) ) {
                    if(r!=null){
                        handler.removeCallbacks(r);
                    }
                    errorPlace.setVisibility(View.VISIBLE);

                }
            }/*else
                handler.removeCallbacks(r);*/

    }


    public static ArrayList<ChannelList> getChannelsWeb(String ar) {

        ArrayList<ChannelList> mList = new ArrayList<ChannelList>();

        JSONArray myjson = null;
        try {
            myjson = new JSONArray(ar);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < myjson.length(); i++) {
            try {
                JSONObject c = myjson.getJSONObject(i);
                String t = c.getString("title");
                String u = c.getString("url");
                String l = c.getString("lang");
                String tp = c.getString("type");
                String m = c.getString("ismenu");
                mList.add(new ChannelList(String.valueOf(i), t, u,l,tp,m));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mList;
    }

    public void flagsStart(int after) {
    switch (after) {
        case AFTER_WEB:
                startAfterWeb=true;
                startAfterChangeCannel=false;
                startAfterReping=false;
                startAfterLostServer=false;
            Log.d(LOG_TAG,"onStart AFTER_WEB");
            break;
        case AFTER_CHANGE_CANNEL:
                startAfterWeb=false;
                startAfterChangeCannel=true;
                startAfterReping=false;
                startAfterLostServer=false;
            Log.d(LOG_TAG,"onStart AFTER_CHANGE_CANNEL");
            break;
        case AFTER_REPING:
                startAfterWeb=false;
                startAfterChangeCannel=false;
                startAfterReping=true;
                startAfterLostServer=false;
            Log.d(LOG_TAG,"onStart AFTER_REPING");
            break;
         case AFTER_LOST_SERVER:
                 startAfterWeb=false;
                 startAfterChangeCannel=false;
                 startAfterReping=false;
                 startAfterLostServer=true;
             Log.d(LOG_TAG,"onStart AFTER_LOST_SERVER");
            break;
    }
}
   /* public void getProgram(String st) {
        programma = new ArrayList<String>();
        String searchId = "", stN = "", curD = "", sercId = "";
        for (int j = 0; j < channelList.size(); j++) {
            stN = channelList.get(j).getChannelNameText();
            if (stN.equals(st)) {
                searchId = channelList.get(j).getChannelId();
                break;
            }
        }
        String curentDate = getCurrentData();
        for (int i = 0; i < programmelList.size(); i++) {
            curD = programmelList.get(i).getDateStart();
            sercId = programmelList.get(i).getChannelId();
            if ((curD.equals(currentDate) && (sercId.equals(searchId)))) {
                String timeStart = programmelList.get(i).getTimeStart();
                String timeStop = programmelList.get(i).getTimeStop();
                programma.add(" " + timeStart + "-" + timeStop + " " + programmelList.get(i).getProgTitleText());
            }
        }
    }*/

    public void initUI() {

        stub = findViewById(R.id.surface_stub);
        mVideoSurface = (SurfaceView) stub.inflate();

        textureStub = findViewById(R.id.texture_stub);
        mVideoTexture = (TextureView) textureStub.inflate();

        tvChannelName = findViewById(R.id.tvChannelName);
        channelPlace = findViewById(R.id.llChannels);
        errorPlace = findViewById(R.id.ll_text_error);
        channelRecycler = findViewById(R.id.rvChannels);
        btSubTitre = findViewById(R.id.btSubTitre);
        btSubTitre.setText(subtitles);

        textError = findViewById(R.id.text_error);
        textError.setText(error_text);

        btExitTV = findViewById(R.id.btExitTV);
        btExitTV.setText(exit_tv);


        placeSub = findViewById(R.id.ll_sub);
        llSub = findViewById(R.id.ll_text_sub);

        mVideoSurfaceFrame = findViewById(R.id.video_surface_frame);
        videoState = findViewById(R.id.video_state);
        tvChannelName.setText("");


    }

    public static int getScreenWidth() {
      // return Resources.getSystem().getDisplayMetrics().widthPixels;
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
    public String getCurrentData() {
        currentTime = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        currentDate = df.format(currentTime.getTime());
        return currentDate;
    }

    public void initPlayer() {
        final ArrayList<String> options = new ArrayList<>();
        mLibVLC = new LibVLC(context, options);//3.0.4 lib
        mMediaPlayer = new MediaPlayer(mLibVLC);
        if (!mChannelType.equals("radio")) {
              mVideoView = mVideoSurface;
           // btSubTitre.setVisibility(View.VISIBLE);
        } else {
           // btSubTitre.setVisibility(View.INVISIBLE);
            mVideoView = mVideoTexture;
        }
        mVideoView.setOnTouchListener(this);
    }

    private static String getResampler() {
        final VLCUtil.MachineSpecs m = VLCUtil.getMachineSpecs();
        return (m == null || m.processors > 2) ? "soxr" : "ugly";
    }
    private static int getDeblocking(int deblocking) {
        int ret = deblocking;
        if (deblocking < 0) {

            VLCUtil.MachineSpecs m = VLCUtil.getMachineSpecs();
            if (m == null)
                return ret;
            if ((m.hasArmV6 && !(m.hasArmV7)) || m.hasMips)
                ret = 4;
            else if (m.frequency >= 1200 && m.processors > 2)
                ret = 1;
            else if (m.bogoMIPS >= 1200 && m.processors > 2) {
                ret = 1;
                //Log.d(TAG, "Used bogoMIPS due to lack of frequency info");
            } else
                ret = 3;
        } else if (deblocking > 4) { // sanity check
            ret = 3;
        }
        return ret;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(LOG_TAG,"onStart");
        // is_menu=false;
        activationChannel=false;
        vlcVout = mMediaPlayer.getVLCVout();
        vlcVout.addCallback(this);
        mMediaPlayer.setEventListener(mPlayerListener);
        if ((mVideoSurface != null)&&!mChannelType.equals("radio")) {
            btSubTitre.setVisibility(View.VISIBLE);
            vlcVout.setVideoView(mVideoSurface);
            vlcVout.attachViews();
            if (mSubtitlesSurfaceView != null)
                vlcVout.setSubtitlesView(mSubtitlesSurfaceView);

        } else {
            btSubTitre.setVisibility(View.INVISIBLE);
            vlcVout.setVideoView(mVideoTexture);
        }



        Media media = new Media(mLibVLC, mUri);
        mMediaPlayer.setMedia(media);
        media.parse();
        Log.d("LOG_TAG","!!!!! media.getMeta = "+ media.getMeta(getTaskId()));

        ////////////////////////////////////////////////////////////////////////
        getSubtitles();
        media.release();

        mLastAudioTrack = mMediaPlayer.getAudioTrack();
        mLastSpuTrack = mMediaPlayer.getSpuTrack();

        mMediaPlayer.play();
        mMediaPlayer.getVideoTrack();


        flagsStart(AFTER_WEB);

       //Log.d(LOG_TAG, "onStart: " + "mCurrentSpuTrack=" + Integer.toString(mCurrentSpuTrack) + " mLastSpuTrack=" + Integer.toString(mLastSpuTrack) + "   ENABLE_SUBTITLES=" + Boolean.toString(ENABLE_SUBTITLES));


        if (mOnLayoutChangeListener == null) {
            mOnLayoutChangeListener = new View.OnLayoutChangeListener() {
                private final Runnable mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        updateVideoSurfaces();
                    }
                };

                @Override
                public void onLayoutChange(View v, int left, int top, int right,
                                           int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
                        mHandler.removeCallbacks(mRunnable);
                        mHandler.post(mRunnable);
                    }
                }
            };
        }
        mVideoSurfaceFrame.addOnLayoutChangeListener(mOnLayoutChangeListener);
    }

    public void setBtnSub(Boolean state) {

        if (mMediaPlayer != null) {

            if (state) {

                btSubTitre.setBackgroundResource(R.drawable.bg_rectangle_radius_white_on);
                btSubTitre.setTextColor(getApplication().getResources().getColor(R.color.black));

                //Log.d(LOG_TAG, "scanSetBtnSub  >0 : " + " mCurrentSpuTrack = " + Integer.toString(mCurrentSpuTrack) + "   ENABLE_SUBTITLES=" + Boolean.toString(ENABLE_SUBTITLES) + "   btnNoActive=" + Boolean.toString(btnNoActive));

            } else {

                btSubTitre.setBackgroundResource(R.drawable.bg_rectangle_radius_white);
                btSubTitre.setTextColor(getApplication().getResources().getColor(R.color.white));
              //  Log.d(LOG_TAG, "scanSetBtnSub else : " + " mCurrentSpuTrack = " + Integer.toString(mCurrentSpuTrack) + "   ENABLE_SUBTITLES=" + Boolean.toString(ENABLE_SUBTITLES) + "   btnNoActive=" + Boolean.toString(btnNoActive));
            }
        }
    }

    public void showMenu(Boolean show) {
        if (show) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            channelRecycler.setLayoutManager(layoutManager);
            channelRecycler.setAdapter(new ChannelRecyclerAdapter(getApplicationContext(), channels));
            channelPlace.setVisibility(View.VISIBLE);
           // programPlace.setVisibility(View.VISIBLE);
            btExitTV.setVisibility(View.VISIBLE);
            placeSub.setVisibility(View.VISIBLE);
            btSubTitreWidth = llSub.getWidth();
            btSubTitreHeight = llSub.getHeight();
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

            );
            //  is_menu = true;
        } else {
            channelPlace.setVisibility(View.INVISIBLE);
           // programPlace.setVisibility(View.INVISIBLE);
            btExitTV.setVisibility(View.INVISIBLE);
            placeSub.setVisibility(View.INVISIBLE);
            btSubTitreWidth = 0;
            btSubTitreHeight = 0;
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                           | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

            );
            // is_menu = false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btExitTV:
                finish();
                break;
            case R.id.btSubTitre:
                setScanTrack();

                if (btnIsSingle && (ENABLE_SUBTITLES == false )) {
                    setBtnSub(true);
                    setOnOffSingleSub(idTrackOn);
                    ENABLE_SUBTITLES = true;
                    PreferencesUtils.saveBoolSharedSetting(getApplicationContext(), CHECK_BTN_SUB, ENABLE_SUBTITLES);
                    checkBtnSub =1;
                    saveChannelState(mFilePatch, mCurrentSpuTrack);
                   // Log.d(LOG_TAG, "btnIsSingle  false " + " mCurrentSpuTrack " + Integer.toString(mCurrentSpuTrack) + "   ENABLE_SUBTITLES=" + Boolean.toString(ENABLE_SUBTITLES));
                    break;
                } else {
                    if (btnIsSingle && (ENABLE_SUBTITLES == true)) {
                        setBtnSub(false);
                       setOnOffSingleSub(idtrackOff);
                        ENABLE_SUBTITLES = false;
                        PreferencesUtils.saveBoolSharedSetting(getApplicationContext(), CHECK_BTN_SUB, ENABLE_SUBTITLES);
                        checkBtnSub =0;
                        saveChannelState(mFilePatch, mCurrentSpuTrack);
                       // Log.d(LOG_TAG, "btnIsSingle  true " + " mCurrentSpuTrack " + Integer.toString(mCurrentSpuTrack) + "   ENABLE_SUBTITLES=" + Boolean.toString(ENABLE_SUBTITLES));
                        break;
                    }
                }
                if (btnIsAll) {
                    onAudioSubClick(v);

                    //Log.d(LOG_TAG,"btnIsAll: "+" mCurrentSpuTrack "+ Integer.toString(mCurrentSpuTrack)+ "   ENABLE_SUBTITLES="+ Boolean.toString(ENABLE_SUBTITLES));
                    break;
                }
                if (btnNoActive) {
                    //onAudioSubClick(v);
                    mCurrentSpuTrack = -2;
                    mMediaPlayer.setSpuTrack(mCurrentSpuTrack);
                    saveChannelState(mFilePatch, mCurrentSpuTrack);
                    setBtnSub(false);
                    //ENABLE_SUBTITLES = false;
                    Toast.makeText(this, getResources().getText(invalid_subtitles), Toast.LENGTH_LONG).show();
                    //Log.d(LOG_TAG,"btnNoActive: "+" mCurrentSpuTrack "+Integer.toString(mCurrentSpuTrack)+ "   ENABLE_SUBTITLES="+ Boolean.toString(ENABLE_SUBTITLES));
                    break;
                }

           // case R.id.bt_en:
               /*  // MyApp.setLocaleEn(RtspPlayerActivity.this);
                PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.LOCALE, "EN");
               finish();
                startActivity(getIntent());
                setLanguege("EN");
                updateUi();*/
              //  break;
//            case R.id.bt_de:
              /*  //MyApp.setLocaleDe(RtspPlayerActivity.this);
                PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.LOCALE, "DE");
                finish();
                startActivity(getIntent());
                setLanguege("DE");
                updateUi();*/
           //     break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int x = (int) event.getX();
        int y = (int) event.getY();
        Log.d(LOG_TAG, "onTouch" + "  x=" + x + "   y=" + y);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP: // нажатие

                if (y < (ScreenHeight - btSubTitreHeight - 100)) {
                    if(!mChannelType.equals("radio")) {

                        if (!is_menu) {
                            //if(!mChannelType.equals("radio")){
                            setScanTrack();
                            // }
                            showMenu(true);
                            is_menu = true;
                        } else {
                            btnIsSingle = false;
                            btnIsAll = false;
                            btnNoActive = false;
                            showMenu(false);
                            is_menu = false;
                        }
                    }else{
                        showMenu(true);
                        is_menu = true;
                    }
                }
                break;
        }
        return true;
    }

    public void setScanTrack() {

        MediaPlayer.TrackDescription[] tracks = mMediaPlayer.getSpuTracks();
        scanTrack(tracks);
       Log.d(LOG_TAG, " nameList =" + nameList+" tracks =" + tracks+" mMediaPlayer =" + mMediaPlayer);

        if (nameList == null) {
          Log.d(LOG_TAG, "Нет субтитров" + "   btnNoActive=" + Boolean.toString(btnNoActive));

            btnNoActive = true;
            btnIsSingle = false;
            btnIsAll = false;
            setBtnSub(false);
        }
        if ((nameList != null) && (nameList.length == 2)) {

            btnIsSingle = true;
            btnIsAll = false;
            btnNoActive = false;
            idTrackOn = idList[1];
            idtrackOff = idList[0];
            switch (checkBtnSub) {
                case 0:
                    setOnOffSingleSub(idtrackOff);
                   saveChannelState(mFilePatch, idtrackOff);
                    setBtnSub(false);
                    break;

                case 1:
                    int saveNumIdTrack=readChannelState(mFilePatch);

                    if (saveNumIdTrack<0){
                        setOnOffSingleSub(idTrackOn);
                        saveChannelState(mFilePatch, idTrackOn);
                        setBtnSub(true);
                    }else{
                        if(saveNumIdTrack>0){

                            setOnOffSingleSub(saveNumIdTrack);
                            setBtnSub(true);
                        }
                    }
                    break;
            }


         //Log.d(LOG_TAG, "1 субтитров" + "   btnNoActive=" + Boolean.toString(btnNoActive));

        }
        if ((nameList != null) && (nameList.length > 2)) {

            btnIsSingle = false;
            btnIsAll = true;
            btnNoActive = false;
           // Log.d(LOG_TAG, ">1 субтитров" + "   btnNoActive=" + Boolean.toString(btnNoActive));

        }
        handler.removeCallbacks(t);
    }

    public void setOnOffSingleSub(int idTrack) {

        MediaPlayer.TrackDescription[] tracks = mMediaPlayer.getSpuTracks();
        mCurrentSpuTrack = -2;
        for (MediaPlayer.TrackDescription track : tracks) {
            if (idTrack == track.id) {
                mCurrentSpuTrack = idTrack;
                break;
            }
        }
        mMediaPlayer.setSpuTrack(mCurrentSpuTrack);

      //Log.d(LOG_TAG, "setOnOffSingleSub " + " idTrack " + Integer.toString(idTrack) + " mCurrentSpuTrack " + Integer.toString(mCurrentSpuTrack));
    }

    public void saveChannelState(String uri, Integer currentSub) {
        channelStateInfo.put(uri, String.valueOf(currentSub));
        PreferencesUtils.saveChannelStateInfo(getApplicationContext(), CHANNEL_INFO, channelStateInfo);
    }

    public int readChannelState(String uri) {
        int num = -2;
        String nums = PreferencesUtils.readChannelStateInfo(getApplicationContext(), CHANNEL_INFO, uri, "-2");
        if (nums != null) {
            num = Integer.valueOf(nums);
        }

        return num;
    }

    private class SubtitlesGetTask extends AsyncTask<String, Void, List<String>> {
        @Override
        protected List<String> doInBackground(String... strings) {
            final String subtitleList_serialized = strings[0];
            List<String> prefsList = new ArrayList<>();

            if (subtitleList_serialized != null) {
                final ByteArrayInputStream bis = new ByteArrayInputStream(subtitleList_serialized.getBytes());
                ObjectInputStream ois = null;
                try {
                    ois = new ObjectInputStream(bis);
                    prefsList = (List<String>) ois.readObject();
                } catch (InterruptedIOException ignored) {
                    return prefsList; /* Task is cancelled */
                } catch (ClassNotFoundException | IOException ignored) {
                } finally {
                    close(ois);
                }
            }

            return prefsList;
        }


        @Override
        protected void onPostExecute(List<String> prefsList) {
            // Add any selected subtitle file from the file picker
            if (prefsList.size() > 0) {

                for (String file : prefsList) {
                    synchronized (mSubtitleSelectedFiles) {
                        if (!mSubtitleSelectedFiles.contains(file))
                            mSubtitleSelectedFiles.add(file);
                    }
                    //Log.d(LOG_TAG, "Adding user-selected subtitle " + file);
                    mMediaPlayer.addSlave(Media.Slave.Type.Subtitle, file, true);
                   //Log.d(LOG_TAG, "SubtitlesGetTask:   =" + "   mMediaPlayer.addSlave " + file);

                   //mMediaPlayer.addSubtitleTrack(file, true);
                }
            }
            mSubtitlesGetTask = null;
        }

        @Override
        protected void onCancelled() {
            mSubtitlesGetTask = null;
        }
    }

    public  boolean close(Closeable closeable) {
        if (closeable != null)
            try {
                closeable.close();
                return true;
            } catch (IOException e) {}
        return false;
    }

    public void getSubtitles() {

        if (mSubtitlesGetTask != null || mMediaPlayer == null)
            return;

        final String subtitleList_serialized =  PreferencesUtils.readStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_SUBTITLE_FILES,null);
        mSubtitlesGetTask = new SubtitlesGetTask();
        mSubtitlesGetTask.execute(subtitleList_serialized);
    }

    public void onAudioSubClick(View v) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.rtsp_customdialog,null);

        final PopupMenu popupMenu = new PopupMenu(this, btSubTitre, Gravity.CENTER,0,R.style.MyPopapTheme);
        final Menu menu = popupMenu.getMenu();

        popupMenu.getMenuInflater().inflate(R.menu.audiosub_tracks, menu);

        menu.findItem(R.id.video_menu_audio_track).setTitle(track_audio);
        menu.findItem(R.id.video_menu_subtitles).setTitle(subtitles);

        menu.findItem(R.id.video_menu_audio_track).setEnabled(mMediaPlayer.getAudioTracksCount() >= 1);
        menu.findItem(R.id.video_menu_subtitles).setEnabled(mMediaPlayer.getSpuTracksCount() > 0);


        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {

                case R.id.video_menu_audio_track:
                    selectAudioTrack();
                    return true;
                case R.id.video_menu_subtitles:
                    selectSubtitles();
                    return true;
                default:
                    return false;
            }

        });
        popupMenu.show();
    }

    private void selectAudioTrack() {
      setESTrackLists();
        selectTrack(mAudioTracksList, mMediaPlayer.getAudioTrack(), track_audio,
                new TrackSelectedListener() {
                    @Override
                    public void onTrackSelected(int trackID) {
                        if (trackID < -1 || mMediaPlayer == null)
                            return;
                        mMediaPlayer.setAudioTrack(trackID);

                    }
                });
    }


    private void selectSubtitles() {
        setESTrackLists();

        selectTrack(mSubtitleTracksList, mMediaPlayer.getSpuTrack(), track_text,
                new TrackSelectedListener() {
                    @Override
                    public void onTrackSelected(int trackID) {
                        if (trackID < -1 || mMediaPlayer == null)
                            return;
                        mMediaPlayer.setSpuTrack(trackID);
                        mCurrentSpuTrack= trackID;
                        saveChannelState(mFilePatch, mCurrentSpuTrack);
                        setBtnSub(mCurrentSpuTrack>2? true:false);
                        PreferencesUtils.saveBoolSharedSetting(getApplicationContext(), CHECK_BTN_SUB, mCurrentSpuTrack>2? true:false);
                    }
                });
        Log.d(LOG_TAG,"selectTrack: "+ " mCurrentSpuTrack "+Integer.toString(mCurrentSpuTrack)+ "   ENABLE_SUBTITLES="+ Boolean.toString(ENABLE_SUBTITLES));
    }

    private  void setESTrackLists() {
        if (mAudioTracksList == null && mMediaPlayer.getAudioTracksCount() > 0)
            mAudioTracksList = mMediaPlayer.getAudioTracks();
        if (mSubtitleTracksList == null && mMediaPlayer.getSpuTracksCount() > 0)
            mSubtitleTracksList = mMediaPlayer.getSpuTracks();
    }

    private void invalidateESTracks(int type) {
        switch (type) {
            case Media.Track.Type.Audio:
                mAudioTracksList = null;
                break;
            case Media.Track.Type.Text:
                mSubtitleTracksList = null;
                break;
        }
    }

    private void setESTracks() {
        if (mLastAudioTrack >= -1) {
            mMediaPlayer.setAudioTrack(mLastAudioTrack);
            mLastAudioTrack = -2;
        }

    }

    void reping() {
        Log.d(LOG_TAG,"reping");
        if((mMediaPlayer!=null)&&(mUri!=null)) {
            mMediaPlayer.stop();

            mMediaPlayer.getVLCVout().detachViews();
            onStart();
            flagsStart(AFTER_REPING);

        }
    }

    void getESAdded() {
        Log.d(LOG_TAG,"getESAdded");
        media = mMediaPlayer.getMedia();

            setESTrackLists();
        int spuTrack = mMediaPlayer.getSpuTracksCount(); /*media.get(MediaWrapper.META_SUBTITLE_TRACK);*/
        if (spuTrack != 0 || mCurrentSpuTrack != -2) {
                mMediaPlayer.setSpuTrack( mCurrentSpuTrack );
           Log.d(LOG_TAG, "getESAdded  =" + "  mCurrentSpuTrack " + Integer.toString(mCurrentSpuTrack));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy" );
        mMediaPlayer.release();
        mLibVLC.release();
        if(r!=null){
            handler.removeCallbacks(r);
        }
        PreferencesUtils.saveBoolSharedSetting(getApplicationContext(), CHECK_BTN_SUB, checkBtnSub==0?false:true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume" );
            handler.postDelayed(r, timer);

       //Log.d(LOG_TAG, "onResume   is_menu =" + Boolean.toString(is_menu) + " mCurrentSpuTrack " + Integer.toString(mCurrentSpuTrack));
        checkBtnSub =PreferencesUtils.readBoolSharedSetting(getApplicationContext(), PreferencesUtils.CHECK_BTN_SUB, false)? 1: 0;
        mVideoView.setOnTouchListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause" );
        saveChannelInPrefs(mFilePatch);
       if(!mChannelType.equals("radio")) {
           if (r != null) {
               handler.removeCallbacks(r);
           }

           PreferencesUtils.saveBoolSharedSetting(getApplicationContext(), CHECK_BTN_SUB, checkBtnSub == 0 ? false : true);
           String subtitleList_serialized = null;
           synchronized (mSubtitleSelectedFiles) {
               if (mSubtitleSelectedFiles.size() > 0) {
                   //Log.d(LOG_TAG, "Saving selected subtitle files");
                   ByteArrayOutputStream bos = new ByteArrayOutputStream();
                   try {
                       ObjectOutputStream oos = new ObjectOutputStream(bos);
                       oos.writeObject(mSubtitleSelectedFiles);
                       subtitleList_serialized = bos.toString();
                   } catch (IOException ignored) {
                   }
               }
           }
           PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_SUBTITLE_FILES, subtitleList_serialized);
           if (mSubtitlesGetTask != null)
               mSubtitlesGetTask.cancel(true);
           if (mOnLayoutChangeListener != null) {
               mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
               mOnLayoutChangeListener = null;
           }
           // Log.d(LOG_TAG, "onPause  is_menu =" + Boolean.toString(is_menu) + "   subtitleList_serialized = " + subtitleList_serialized + "  mCurrentSpuTrack " + Integer.toString(mCurrentSpuTrack));
       }//если не радио
            mMediaPlayer.stop();

            mMediaPlayer.getVLCVout().detachViews();
     //   }//если не радио
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "onStop" );
        saveChannelInPrefs(mFilePatch);
        if (r != null) {
            handler.removeCallbacks(r);
        }
       if(!mChannelType.equals("radio")) {


            PreferencesUtils.saveBoolSharedSetting(getApplicationContext(), CHECK_BTN_SUB, checkBtnSub == 0 ? false : true);
            String subtitleList_serialized = null;
            synchronized (mSubtitleSelectedFiles) {
                if (mSubtitleSelectedFiles.size() > 0) {
                    //Log.d(LOG_TAG, "Saving selected subtitle files");
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    try {
                        ObjectOutputStream oos = new ObjectOutputStream(bos);
                        oos.writeObject(mSubtitleSelectedFiles);
                        subtitleList_serialized = bos.toString();
                    } catch (IOException ignored) {
                    }
                }
            }
            PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.VIDEO_SUBTITLE_FILES, subtitleList_serialized);
            if (mSubtitlesGetTask != null)
                mSubtitlesGetTask.cancel(true);
            if (mOnLayoutChangeListener != null) {
                mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
                mOnLayoutChangeListener = null;
            }
       }//если не радио
            mMediaPlayer.stop();
            //Log.d(LOG_TAG, "onStop: is_menu =" + Boolean.toString(is_menu) + "  subtitleList_serialized = " + subtitleList_serialized + " mCurrentSpuTrack" + Integer.toString(mCurrentSpuTrack));
            mMediaPlayer.getVLCVout().detachViews();
      //  }//если не радио

    }
    private static class MyPlayerListener implements MediaPlayer.EventListener {
        private WeakReference<RtspPlayerActivity> mOwner;

        public MyPlayerListener(RtspPlayerActivity owner) {
            mOwner = new WeakReference<RtspPlayerActivity>(owner);
        }

        @Override
        public void onEvent(MediaPlayer.Event event) {
            RtspPlayerActivity player = mOwner.get();

            switch (event.type) {
                case MediaPlayer.Event.EndReached:
                    player.reping();
                   Log.d(player.LOG_TAG,"EndReached");
                    break;
                case MediaPlayer.Event.Playing:
                   Log.d(player.LOG_TAG,"Playing");
                    player.activationChannel=true;
                    break;
                case MediaPlayer.Event.Paused:
               Log.d(player.LOG_TAG,"Paused");
                    break;
                case MediaPlayer.Event.MediaChanged:
                  Log.d(player.LOG_TAG,"MediaChanged");
                    break;
                case MediaPlayer.Event.EncounteredError:
                    Log.d(player.LOG_TAG,"EncounteredError");
                    player.activationChannel=false;
                    break;
                case MediaPlayer.Event.Opening:
                   Log.d(player.LOG_TAG,"PositionChanged");
                    break;
                case MediaPlayer.Event.Stopped:
                    Log.d(player.LOG_TAG,"Stopped");
                    //player.activationChannel=false;

                break;
                case MediaPlayer.Event.Buffering:
                   Log.d(player.LOG_TAG,"Buffering");
                    break;
                case MediaPlayer.Event.ESAdded:
                    Log.d(player.LOG_TAG,"ESAdded");
                  if (event.getEsChangedType() == Media.Track.Type.Text) {
                            player.getESAdded();
                        }

                case MediaPlayer.Event.ESDeleted:
                    Log.d(player.LOG_TAG,"ESDeleted");
                     //if (event.getEsChangedType() == Media.Track.Type.Video) {
                       player.invalidateESTracks(event.getEsChangedType());
                  //  }

                    break;
                case MediaPlayer.Event.ESSelected:
                   Log.d(player.LOG_TAG,"ESSelected");
                    if (event.getEsChangedType() == Media.Track.Type.Text) {
                        player.getESAdded();
                    }

                    break;


                default:
                    break;
            }
        }
    }



    public void scanTrack(final MediaPlayer.TrackDescription[] tracks) {
        nameList = null;
        idList = null;
        if (tracks == null) {

            return;
        }
        String[] nameList1 = new String[tracks.length];
        int[] idList1 = new int[tracks.length];
        int i = 0;
        int colRemPos = 0;
        int listPosition = 0;
        for (MediaPlayer.TrackDescription track : tracks) {
            if ((track.id < 30) || (track.id > 100)) {
                colRemPos++;
            }
            idList1[i] = track.id;
            nameList1[i] = track.name;
            // map the track position to the list position
            if (track.id == currentTrack)
                listPosition = i;
            i++;
            // }else{
        }

        nameList = new String[colRemPos];
        idList = new int[colRemPos];
        i = 0;
        int j = 0;
        // skip technical sub
        while (j < tracks.length) {
            if ((idList1[j] < 30) || (idList1[j] > 100)) {
                nameList[i] = nameList1[j];
                idList[i] = idList1[j];
                i++;
            }
            j++;
        }
    }

    private interface TrackSelectedListener {
        void onTrackSelected(int trackID);
    }

    private void selectTrack(final MediaPlayer.TrackDescription[] tracks, int currentTrack, int titleId,
                             final TrackSelectedListener listener) {
        if (listener == null)
            throw new IllegalArgumentException("listener must not be null");

        scanTrack(tracks);
        if (!isFinishing()) {
            mAlertDialog = new AlertDialog.Builder(RtspPlayerActivity.this,R.style.MyDialogTheme)
                    .setTitle(titleId)
                    .setSingleChoiceItems(nameList, listPosition, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int listPosition) {
                            int trackID = -1;
                            // Reverse map search...
                            for (MediaPlayer.TrackDescription track : tracks) {
                                if (idList[listPosition] == track.id) {
                                    trackID = track.id;
                                    break;
                                }
                            }
                           /* setOnOffSingleSub(mCurrentSpuTrack);
                            saveChannelState(mFilePatch, mCurrentSpuTrack);
                            setBtnSub(mCurrentSpuTrack>2? true:false);*/

                            listener.onTrackSelected(trackID);
                            dialog.dismiss();
                        }
                    })
                    .create();
            mAlertDialog.setCanceledOnTouchOutside(true);
            mAlertDialog.setOwnerActivity(RtspPlayerActivity.this);
            mAlertDialog.show();
        }
    }

    private void updateVideoSurfaces() {
        int sw = getWindow().getDecorView().getWidth();
        int sh = getWindow().getDecorView().getHeight();

        // sanity check
        if (sw * sh == 0) {
            //Log.e(TAG, "Invalid surface size");
            return;
        }

        mMediaPlayer.getVLCVout().setWindowSize(sw, sh);

        ViewGroup.LayoutParams lp = mVideoView.getLayoutParams();
        if (mVideoWidth * mVideoHeight == 0) {
            /* Case of OpenGL vouts: handles the placement of the video using MediaPlayer API */
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoView.setLayoutParams(lp);
            lp = mVideoSurfaceFrame.getLayoutParams();
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoSurfaceFrame.setLayoutParams(lp);
            return;
        }

        if (lp.width == lp.height && lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {

        }

        double dw = sw, dh = sh;

        final boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        if (sw > sh && isPortrait || sw < sh && !isPortrait) {
            dw = sh;
            dh = sw;
        }

        // compute the aspect ratio
        double ar, vw;
        if (mVideoSarDen == mVideoSarNum) {
            /* No indication about the density, assuming 1:1 */
            vw = mVideoVisibleWidth;
            ar = (double)mVideoVisibleWidth / (double)mVideoVisibleHeight;
        } else {
            /* Use the specified aspect ratio */
            vw = mVideoVisibleWidth * (double)mVideoSarNum / mVideoSarDen;
            ar = vw / mVideoVisibleHeight;
        }

        // compute the display aspect ratio
        double dar = dw / dh;

        switch (CURRENT_SIZE) {
            case SURFACE_BEST_FIT:
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_FIT_SCREEN:
                if (dar >= ar)
                    dh = dw / ar; /* horizontal */
                else
                    dw = dh * ar; /* vertical */
                break;
            case SURFACE_FILL:
                break;
            case SURFACE_16_9:
                ar = 16.0 / 9.0;
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_4_3:
                ar = 4.0 / 3.0;
                if (dar < ar)
                    dh = dw / ar;
                else
                    dw = dh * ar;
                break;
            case SURFACE_ORIGINAL:
                dh = mVideoVisibleHeight;
                dw = vw;
                break;
        }

        // set display size
        lp.width  = (int) Math.ceil(dw * mVideoWidth / mVideoVisibleWidth);
        lp.height = (int) Math.ceil(dh * mVideoHeight / mVideoVisibleHeight);
        mVideoView.setLayoutParams(lp);
        if (mSubtitlesSurfaceView != null)
            mSubtitlesSurfaceView.setLayoutParams(lp);

        // set frame size (crop if necessary)
        lp = mVideoSurfaceFrame.getLayoutParams();
        lp.width = (int) Math.floor(dw);
        lp.height = (int) Math.floor(dh);
        mVideoSurfaceFrame.setLayoutParams(lp);

        mVideoView.invalidate();
        if (mSubtitlesSurfaceView != null)
            mSubtitlesSurfaceView.invalidate();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)

    public void onNewVideoLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {
        mVideoWidth = width;
        mVideoHeight = height;
        mVideoVisibleWidth = visibleWidth;
        mVideoVisibleHeight = visibleHeight;
        mVideoSarNum = sarNum;
        mVideoSarDen = sarDen;
        updateVideoSurfaces();
    }
    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    private class ParseTracksTask extends AsyncTask<Void, Void, Media> {

        @Override
        protected Media doInBackground(Void... params) {

           // final LibVLC libVlc = VLCInstance.get();
            if (mLibVLC == null || isCancelled())
                return null;

            Media media = new Media(mLibVLC,  mUri);
            media.parse();

            return media;
        }

        @Override
        protected void onPostExecute(Media media) {
            mParseTracksTask = null;
            if (media == null || isCancelled())
                return;
            boolean hasSubs = false;
            media = new Media(mLibVLC,  mUri);
            media.parse();
            final int trackCount = media.getTrackCount();
            List<Media.Track> tracks = new LinkedList<>();
            for (int i = 0; i < trackCount; ++i) {
                final Media.Track track = media.getTrack(i);
                tracks.add(track);
                hasSubs |= track.type == Media.Track.Type.Text;
            }
            media.release();
            mAdapter.setTracks(tracks);

        }

        @Override
        protected void onCancelled() {
            mParseTracksTask = null;
        }
    }
}
