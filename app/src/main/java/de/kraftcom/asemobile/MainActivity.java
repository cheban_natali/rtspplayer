package de.kraftcom.asemobile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import de.kraftcom.asemobile.BuildConfig;
import de.kraftcom.asemobile.R;
import de.kraftcom.asemobile.api.Api;
import de.kraftcom.asemobile.api.Result;
import de.kraftcom.asemobile.model.ChannelList;
import de.kraftcom.asemobile.model.DataBaseHandel;
import de.kraftcom.asemobile.model.modelEpg.ChannelEpg;
import de.kraftcom.asemobile.model.modelEpg.Programme;
import de.kraftcom.asemobile.util.PreferencesUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText textUrlRtsp;
    Button btnOpenRtspPlayer, btnOpenParseFail, btnLoadParseFail, btnUnzipParseFail;
    //ArrayList<ChannelList> channels;
    static final String TEXT_URL="TEXT_URL";
    static final String LIST_URL="LIST_URL";
    public String uriFail;
    final String LOG_TAG = "myLogs";
    InputStream is;
    public String st1,st2,st3;

    public String dataStopText, dataStartText, timeStartText, timeStopText;
    public ChannelEpg parsChanel;
    public ArrayList<ChannelEpg> channelListEpg;
    public static List<ChannelList> channelList;
    public Programme parsProgramm;
    public ArrayList<Programme> programmelList;
    List<Programme>  programmelListForAnaliz;
    // SQLiteDatabase db;
    public DataBaseHandel dataBaseHandel;
    SQLiteDatabase db;
    Cursor userCursor;
    String currentDate;
    Calendar currentTime;
    WebView mWebView;
    JSInterface mJSInterface;
    SwipeRefreshLayout mySwipeRefreshLayout;
    SharedPreferences prefs;
    public String loc;
    boolean isError = false,isagain=false;
    ImageView btReping, btSettings;
    RelativeLayout layout;
    String urlSait;

    public long timerT=15000;

    Handler handler = new Handler();

    final Runnable err = new Runnable() {
        public void run() {
            handler.postDelayed(this, timerT);
            offlainLayout();

        }
    };
    final Runnable again = new Runnable() {
        public void run() {
            handler.postDelayed(this, timerT);
            onceAgain();

        }
    };


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //downloadNewFail();
        final String AUTHORITY = BuildConfig.APPLICATION_ID;
        prefs = getSharedPreferences(PreferencesUtils.PREFERENCES_FILE, 0);
        PreferencesUtils.saveStrSharedSetting(getApplicationContext(), PreferencesUtils.LOCALE, "EN");
        loc = PreferencesUtils.readStrSharedSetting(getApplicationContext(), PreferencesUtils.LOCALE, null);
        layout = (RelativeLayout)findViewById(R.id.error);
        btReping= findViewById(R.id.bt_reping);
        btSettings= findViewById(R.id.bt_settings);
        layout.setVisibility(View.GONE);
        btReping.setOnClickListener(this);
        btSettings.setOnClickListener(this);

        mWebView = findViewById(R.id.web);

        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setAppCacheEnabled(false);
        mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mWebView.setWebChromeClient(new MyChromeClient());

        mWebView.setWebViewClient(new WebViewClient(){
            @Override public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return true; } });
        mJSInterface = new JSInterface(this);
        mWebView.addJavascriptInterface(mJSInterface, "Android");

         urlSait="http://asemobile.kraftcom.de/index.html";

        //  urlSait="http://reed.pe.hu/chanels1/index.html";

        //    urlSait="http://reed.pe.hu/chanels/index.html";// 3-й параметр lang

       mWebView.loadUrl(urlSait);
        //   mWebView.loadUrl("http://auth.kraftcom.de/mobile/index.html");
       // mWebView.loadUrl("http://reed.pe.hu/chanels1/index.html");
        //   mWebView.loadUrl("http://185.74.216.225:12800/mobile/");
       // mWebView.loadUrl("http://reed.pe.hu/chanels/index.html");
        mWebView.setWebViewClient(new WebViewClient(){

            public void onPageFinished(WebView view, String url) {
                    mWebView.loadUrl("javascript:callFromActivity('" + AUTHORITY + "')");
            }

            @Override
            public void onReceivedError(final WebView view, int errorCode, String description,
                                        final String failingUrl) {

               if(!isagain){
                //   Log.d("log", "Start onceAgain  isagain= "+ isagain);
                handler.postDelayed(again, timerT);
               }else{
                 //  Log.d("log", "Start err   isagain="+ isagain);
                   handler.postDelayed(err, timerT);
               }

            }
        });


        dataBaseHandel = new DataBaseHandel(this);
        db = dataBaseHandel.getReadableDatabase();
        programmelListForAnaliz = dataBaseHandel.getAllProgramme();
        db.close();


        if(programmelListForAnaliz.size()==0) {// table Programs is empty

            addOrRefreshBase();
        }else{
          Boolean  actulityBase=false;
          String curD =getCurrentData();
            for(int i=0; i< programmelListForAnaliz.size(); i++){
               String baseDate=programmelListForAnaliz.get(i).getDateStart();
                if(baseDate.equals(curD)){
                    actulityBase=true; break;
                }
            }
            if (actulityBase==false){
                addOrRefreshBase();
            }
        }
        checkVersion();

    }

    public void offlainLayout(){
        Log.d("log", "offlainLayout + isagain="+ isagain);
        mWebView.setVisibility(View.GONE);
        layout.setVisibility(View.VISIBLE);
        handler.removeCallbacks(err);
    }
    public void onceAgain(){

        handler.removeCallbacks(again);
        isagain=true;
        mWebView.reload();
        Log.d("log", "onceAgain + isagain="+ isagain);
    }

    private void checkVersion() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://chat-d99dd.firebaseio.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);


        // Call<Result> call = api.check("1");
        Call<Result> call = api.check(BuildConfig.VERSION_NAME);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {

                if(response.isSuccessful()&&response.body().needUpdate){
                    showDialog();

                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }
    private void showDialog() {
        int dialog_message, dialog_title, update, latter;
        if (loc == null) {
            dialog_message = R.string.dialog_message;
            dialog_title = R.string.dialog_title;
            update = R.string.update;
            latter = R.string.latter;
        } else {
            if (loc.equals("EN")) {
                dialog_message = R.string.dialog_message;
                dialog_title = R.string.dialog_title;
                update = R.string.update;
                latter = R.string.latter;
            } else {
                dialog_message = R.string.dialog_message;
                dialog_title = R.string.dialog_title;
                update = R.string.update;
                latter = R.string.latter;
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(dialog_title));
        builder.setMessage(getString(dialog_message));
        builder.setPositiveButton(getString(update),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
                    }
                });

        builder.setNegativeButton(getString(latter), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bt_reping:
                mWebView.setVisibility(View.VISIBLE);
                layout.setVisibility(View.GONE);
                mWebView.loadUrl(urlSait);

                break;
            case R.id.bt_settings:
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                break;
        }


    }

    public void addOrRefreshBase(){
        new Thread(new Runnable() {
            public void run() {
                //URL url = new URL("http://programtv.ru/xmltv.xml.gz");
                //URL url = new URL("https://archive.org/download/xmltv_201802/xmltv.xml");

                String fileProgramURL="http://programtv.ru/xmltv.xml.gz";
                String mTGFileName = getFileName(fileProgramURL);

                DownloadFromUrl(fileProgramURL,mTGFileName);
                File mTgfile = new File(getApplication().getFilesDir().getPath().toString(),mTGFileName);// fail where was load tg zip file
                String mUnZipFailName=getDstFileName(mTgfile.getPath());// get ful name fail unzip fail for save

                UnZipNewFail(mTgfile, mUnZipFailName);
                parseXmlFail(mUnZipFailName);
                deleteFailTgXml(mTGFileName,mUnZipFailName );

            }
        }).start();
    }

    class JSInterface {
        Context mContext;
        HashMap<String, String> mObjectsFromJS = new HashMap<String, String>();
        HashMap<String, Boolean> mObjectsBulFromJS = new HashMap<String, Boolean>();
        HashMap<String, JSONObject> ArrayFromJS = new HashMap<String, JSONObject>();
        Intent intent = new Intent(MainActivity.this, RtspPlayerActivity.class);
        Intent intenthttp = new Intent(MainActivity.this, VideoPlayer.class);

        JSInterface(Context c){mContext=c;}

        @JavascriptInterface
        public void openurl(String url) {
            try{
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                mContext.startActivity(browserIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this.mContext, "No application can handle this request,"
                        + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        /////////////// 2 param
       /* public void passObject(String name, String url) {
            if (!url.startsWith("http")) {
                intent.putExtra("TEXT_URL", url);
                intent.putExtra("TEXT_NAME", name);
                startActivity(intent);
                //  finish();
                mObjectsFromJS.put(name, url);
            } else {
                intenthttp.putExtra("TEXT_URL", url);
                intenthttp.putExtra("TEXT_NAME", name);
                startActivity(intenthttp);
            }
        }*/
        ///////////////// 3 param
        public void passObject(String name, String url, String lang, String type, String ismenu) {

            if(type.equals("tv")|| type.equals("radio")|| url.contains("/udp/")){
                Log.d(LOG_TAG,"Start tv");
                mObjectsFromJS.put(name, mObjectsFromJS.put(url, mObjectsFromJS.put(lang,mObjectsFromJS.put(type,ismenu))));
                intent.putExtra("TEXT_URL", url);
                intent.putExtra("TEXT_NAME", name);
                intent.putExtra("TEXT_LANG", lang);
                intent.putExtra("TEXT_TYPE", type);
                intent.putExtra("IS_MENU", ismenu);
                startActivity(intent);


        }
            else
            {
                Log.d(LOG_TAG,"Start Video");
                intenthttp.putExtra("TEXT_URL", url);
                intenthttp.putExtra("TEXT_NAME", name);
                intenthttp.putExtra("TEXT_LANG", lang);
                intent.putExtra("TEXT_TYPE", type);
                intent.putExtra("IS_MENU", ismenu);
                startActivity(intenthttp);
            }
        }

        @JavascriptInterface
        public void passList(String name, String json) {
            try {
                JSONArray myjson = new JSONArray(json);
                if (myjson!=null){
                    Bundle b=new Bundle();
                    b.putString("Array", myjson.toString());
                    intent.putExtras(b);
                    //MainActivity.getChannelsWeb(myjson);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void startApp(String app) {
            try {
                Intent LaunchIntent = mContext.getPackageManager().getLaunchIntentForPackage(app);
               // ActivityManager am = (ActivityManager) mContext.getSystemService(Activity.ACTIVITY_SERVICE);
                //String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
               // Log.d(LOG_TAG, packageName);
                mContext.startActivity(LaunchIntent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        @JavascriptInterface
        public void killApp(String app) {
            ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
            am.killBackgroundProcesses(app);
        }
    }
    public class MyChromeClient extends WebChromeClient {
        private CustomViewCallback myCallback = null;
        private View myView = null;

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
           // Log.v("Testing", "onShowCustomView");
            if (myCallback != null)
            {
                myCallback.onCustomViewHidden();
                myCallback = null;
                return;
            }
            long id = Thread.currentThread().getId();
           // Log.v("Testing", "WidgetChromeClient rong debug in showCustomView Ex: " + id);
           // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            // Hide the nav bar and status bar
                           | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                    //  | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                                   | View.SYSTEM_UI_FLAG_IMMERSIVE
            );

            ViewGroup parent = (ViewGroup) mWebView.getParent();
            String s = parent.getClass().getName();
          //  Log.v("Testing", "WidgetChromeClient rong debug Ex: " + s);
            parent.removeView(mWebView);
            parent.addView(view);

            myView = view;
            myCallback = callback;

        }

        @Override
        public void onHideCustomView() {

         //   Log.v("Testing", "onHideCustomView");
            long id = Thread.currentThread().getId();
          //  Log.v("Testing", "WidgetChromeClient rong debug in hideCustom Ex: " + id);

            if (myView != null)
            {
                if (myCallback != null)
                {
                    myCallback.onCustomViewHidden();
                    myCallback = null;
                }

                ViewGroup parent = (ViewGroup) myView.getParent();
                parent.removeView(myView);
                parent.addView(mWebView);
                myView = null;

                getWindow().getDecorView().setSystemUiVisibility(
                                 View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                              //  | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                             //  | View.SYSTEM_UI_FLAG_FULLSCREEN
                        //    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                         | View.SYSTEM_UI_FLAG_IMMERSIVE

                );
            }
        }
    }

    ///////////////////////////////////////////////////////////
    public String getCurrentData(){
        currentTime = Calendar.getInstance();
        SimpleDateFormat df=new SimpleDateFormat("yyyyMMdd");
        currentDate=df.format(currentTime.getTime());
        return currentDate;
    }
    public String getDstFileName(final String srcFileName) {
        return srcFileName.substring(0, srcFileName.lastIndexOf("."));
    }
    public String getFileName(final String srcFileName) {
        return srcFileName.substring(srcFileName.lastIndexOf("/")+1,srcFileName.length());
    }

    public void deleteFailTgXml(String mTGFileName,String mUnZipFailName ){
        File file = new File(mTGFileName);
        if (file.exists()) {
            file.delete();
            //Log.d("file", "Fail Delete mTGFileName");
        }
        file = new File(mUnZipFailName);
        if (file.exists()) {
            file.delete();
            //Log.d("file", "Fail Delete mUnZipFailName");
        }
    }
    public void DownloadFromUrl(String FileUrl, String fileName) {

        URL url;
        int fileLength;
        InputStream input1 = null;

        File file = new File(getApplication().getFilesDir().getPath().toString(),fileName);
         try {
            file.createNewFile(); //create - rewrite
            url = new URL(FileUrl);
            //Open a connection to that URL.
            URLConnection ucon = url.openConnection();
            ucon.connect();
            fileLength = ucon.getContentLength();
            input1 = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] data = new byte[1024];
            int current = 0;
            while ((current = input1.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, current);
            }
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(buffer.toByteArray());
            fos.flush(); fos.close();
            input1.close();
             //Log.d("file", "Fail DownloadFromUrl");
        } catch (IOException e) {
        }
    }

    public void UnZipNewFail(File file, String mUnZipFailName){

        if(file.exists()){
            byte[] buffer = new byte[1024];
            GZIPInputStream  gzipis = null;
            try {
                gzipis = new GZIPInputStream(new FileInputStream(file));
                FileOutputStream fos = new FileOutputStream(mUnZipFailName);
                int length = 0;
                while ((length = gzipis.read(buffer)) > 0) {
                    fos.write(buffer, 0, length);
                }
                fos.flush();
                fos.close();
                //Log.d("file", "Fail UnZipNewFail");
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
    }

    public void parseDate(String st){
        //  20180125 025500 +0300
        st1 = st.substring(0,8);
        st2 = st.substring(8,14);
        st3="";
        if (st.indexOf("+")>0){
            st3 = st.substring(st.indexOf("+")+1,st.length());
        }
    }

    public String parseTimeSt(String st){
        String st4="";
        st4=st.substring(0,2)+":"+st.substring(2,4);
        return st4;


    }

    public void parseXmlFail(String fileName) {
        String tmp = "";
        String    id_channel,lang ,text,ls1, ls2, ls3;
        int flg=0;
        parsChanel = new ChannelEpg();
        parsProgramm = new Programme();
        channelListEpg = new ArrayList<ChannelEpg>();
        programmelList = new ArrayList<Programme>();
        dataBaseHandel = new DataBaseHandel(this);
        db = dataBaseHandel.getWritableDatabase();
        db.execSQL(" DROP TABLE IF EXISTS " + DataBaseHandel.TAB_CHANNELS);////////////////
        db.execSQL(" DROP TABLE IF EXISTS " + DataBaseHandel.TAB_PROGRAMMES);////////////////
        dataBaseHandel.onCreate(db); // Create tables again
            try {
                 XmlPullParser xpp = prepareXpp(fileName);

                while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                    switch (xpp.getEventType()) {
                        // начало документа
                        case XmlPullParser.START_DOCUMENT:
                            ////Log.d(LOG_TAG, "START_DOCUMENT");
                            break;
                        // начало тэга
                        case XmlPullParser.START_TAG:

                            if (xpp.getName().equals("channel")) {
                                flg = 1;

                                parsChanel.setChannelId(xpp.getAttributeValue(null, "id"));
                                break;
                            }

                            if (xpp.getName().equals("display-name")) {
                                flg = 11;
                                parsChanel.setChannelNameLang(xpp.getAttributeValue(null, "lang"));
                                break;
                            }
                            if (xpp.getName().equals("icon")) {
                                flg = 1;
                                parsChanel.setChannelIconSrc(xpp.getAttributeValue(null, "src"));
                                break;
                            }
                            if (xpp.getName().equals("programme")) {
                                flg = 2;
                                parseDate(xpp.getAttributeValue(null, "start"));
                                parsProgramm.setDateStart(st1);
                                parsProgramm.setTimeStart(parseTimeSt(st2));
                                parsProgramm.setTimeSdvig(parseTimeSt(st3));

                                parseDate(xpp.getAttributeValue(null, "stop"));
                                parsProgramm.setDateStop(st1);
                                parsProgramm.setTimeStop(parseTimeSt(st2));

                                // parsProgramm.setDateStop(xpp.getAttributeValue(null,"stop"));
                                parsProgramm.setChannelId(xpp.getAttributeValue(null, "channel"));
                                break;
                            }
                            if (xpp.getName().equals("title")) {
                                flg = 12;
                                parsProgramm.setProgTitleLang(xpp.getAttributeValue(null, "lang"));
                                break;
                            }
                            if (xpp.getName().equals("category")) {
                                flg = 3;
                                //parsProgramm.setProgTitleLang(xpp.getAttributeValue(null,"lang"));
                                break;
                            }
                            if (xpp.getName().equals("desc")) {
                                flg = 4;
                                //parsProgramm.setProgTitleLang(xpp.getAttributeValue(null,"lang"));
                                break;
                            }
                            break;

                        // конец тэга
                        case XmlPullParser.END_TAG:

                            if (xpp.getName().equals("channel")) {
                                parsChanel.setChannelUrl("");
                                channelListEpg.add(parsChanel);
                                dataBaseHandel.addChannel(parsChanel);
                                ////Log.d("pars", "END_TAG: name = " + xpp.getName() + "= " + parsChanel);
                            }
                            if (xpp.getName().equals("programme")) {

                             /*parsProgramm.setProgCategoryLang("");
                             parsProgramm.setProgCategoryText("");
                             parsProgramm.setProgDescLang("");
                             parsProgramm.setProgDescText("");*/
                                programmelList.add(parsProgramm);
                                dataBaseHandel.addProgramme(parsProgramm);

                                ////Log.d("pars", "END_TAG: name = " + xpp.getName() + "= " + parsProgramm);
                            }
                            break;

                        // содержимое тэга
                        case XmlPullParser.TEXT:
                            if (flg == 11) {

                                parsChanel.setChannelNameText(xpp.getText().toString());
                                flg = 1;
                                break;
                            }
                            if (flg == 12) {
                                parsProgramm.setProgTitleText(xpp.getText().toString());
                                flg = 2;
                                break;
                            }


                        default:
                            break;
                    }
                    // следующий элемент
                    xpp.next();
                }
                //Log.d("file", "Parse Ok");

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
      //  }
        db.close();
       // userCursor.close();

    }

    public XmlPullParser prepareXpp (String fileName) throws XmlPullParserException{
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();
        File file = new File(fileName);
        if (file.exists()) {
            try {
                FileInputStream fis = null;
                fis = new FileInputStream(file);
                parser.setInput(new InputStreamReader(fis));

            } catch (FileNotFoundException e)
            {
                e.printStackTrace();   }
        }
        return parser;
    }

}
