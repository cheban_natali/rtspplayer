package de.kraftcom.asemobile.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.kraftcom.asemobile.R;
import de.kraftcom.asemobile.model.ChannelList;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;

/**
 * Created by cheba on 20.01.2018.
 */

public class ChannelRecyclerAdapter extends RecyclerView.Adapter<ChannelRecyclerAdapter.ViewHolder>{
    private ArrayList<ChannelList> channelLists;
    private Context ctx;
    private MediaPlayer mMediaPlayer;
    private LibVLC mLibVLC;

    public ChannelRecyclerAdapter(Context ctx, ArrayList<ChannelList> channelLists) {
        this.channelLists = channelLists;
        this.ctx = ctx;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        public TextView tvChannelName;
        public TextView tvChannelNum;


        public ViewHolder(View itemView) {
            super(itemView);
            tvChannelNum=itemView.findViewById(R.id.tvChannelNum);
            tvChannelName=itemView.findViewById(R.id.tvChannelName);
        }
    }

    @Override
    public ChannelRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId = R.layout.item_channel_list;
        View v= LayoutInflater.from(parent.getContext()).inflate(layoutId,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(getItemCount()>0) {
            holder.tvChannelNum.setText(String.valueOf(position +1));
            holder.tvChannelName.setText(String.valueOf(channelLists.get(position).getChannelTitle()));

        }else{
            //Toast.makeText(ctx, "Списка каналов нет", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public int getItemCount() {
        return channelLists.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
