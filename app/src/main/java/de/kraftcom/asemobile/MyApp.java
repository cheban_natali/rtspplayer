package de.kraftcom.asemobile;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class MyApp extends Application{
    public final String LOG_TAG = "Player";
    public void onCreate() {
        super.onCreate();
    }
}

    /*    if (isExternalStorageWritable()) {

            File appDirectory = new File(Environment.getExternalStorageDirectory() + "/AsemobileKraftcom");
            File logDirectory = new File(appDirectory + "/log");
            File logFile = new File(logDirectory, "logcat" + System.currentTimeMillis() + ".txt");
            Log.d(LOG_TAG, "appDirectory = "+ appDirectory);
            Log.d(LOG_TAG, "logDirectory = "+ logDirectory);
            Log.d(LOG_TAG, "logFile = "+ logFile);

            // create app folder
            if (!appDirectory.exists()) {
                Log.d(LOG_TAG, "appDirectory = "+ appDirectory.exists());
                appDirectory.mkdir();
                Log.d(LOG_TAG, "appDirectory = "+ appDirectory.exists());
            }

            // create log folder
            if (!logDirectory.exists()) {
                Log.d(LOG_TAG, "logDirectory = "+ logDirectory.exists());
                logDirectory.mkdir();
                Log.d(LOG_TAG, "logDirectory = "+ logDirectory.exists());

            }

            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f " + logFile);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (isExternalStorageReadable()) {
            // only readable
        } else {
            // not accessible
        }
    }
        *//* Checks if external storage is available for read and write *//*
        public boolean isExternalStorageWritable() {
            String state = Environment.getExternalStorageState();
            if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
                Log.d(LOG_TAG, "isExternalStorageWritable = "+ true);
                return true;
            }
            Log.d(LOG_TAG, "isExternalStorageWritable = "+ false);
            return false;
        }

        *//* Checks if external storage is available to at least read *//*
        public boolean isExternalStorageReadable() {
            String state = Environment.getExternalStorageState();
            if ( Environment.MEDIA_MOUNTED.equals( state ) ||
                    Environment.MEDIA_MOUNTED_READ_ONLY.equals( state ) ) {
                Log.d(LOG_TAG, "isExternalStorageReadable = "+ true);
                return true;
            }
            Log.d(LOG_TAG, "isExternalStorageReadable = "+ false);
            return false;
        }
}*/
